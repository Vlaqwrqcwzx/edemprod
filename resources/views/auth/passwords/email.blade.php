@extends('layouts.front.app')

@section('content')
    <div class="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <ul class="breadcrumbs">
                    <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="{{ route('home') }}" itemprop="url">Главная</a>
                    </li> <li class="sep">&gt;</li>
                    <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a href="{{ route('login') }}" itemprop="url">Авторизация</a>
                    </li> <li class="sep">&gt;</li>
                    <li class="current">Восстановление пароля</li>
                </ul><!-- .breadcrumbs -->
            </div>
        </div>
    </div>
    <section class="shop">
        <div class="container">
            <div class="row">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <main class="col-xs-12 col-sm-12">
                    <div class="recovery-form-wrapper">

                        <div class="recovery-form">
                            <h1>Восстановление пароля</h1>

                            <form role="form" method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}
                                <div class="text-center recovery-form-desc">
                                    Введите адрес электронной почты, который был использован при регистрации.<br>
                                    На вашу почту будет отправлен новый пароль для восстановления доступа в аккаунт.
                                </div>

                                <div class="center-block recovery-form-main">
                                    <label for="login">E-mail</label>
                                    <input class="form-control" type="email" id="login" name="email" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                    <div class="recovery-form-footer">
                                        <button type="submit" class="btn btn-primary">Восстановить пароль</button>
                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>
                </main>
            </div>
        </div>
    </section>
@endsection
