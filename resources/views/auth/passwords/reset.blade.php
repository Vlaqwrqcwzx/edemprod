@extends('layouts.front.app')

@section('content')
    <section class="shop">
        <!-- START: BREADCRUMBS -->
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumbs">
                        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{ route('home') }}" itemprop="url">Главная</a>
                        </li>
                        <li class="sep">&gt;</li>
                        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{ route('login') }}" itemprop="url">Авторизация</a>
                        </li>
                        <li class="sep">&gt;</li>
                        <li class="current">Сброс пароля</li>
                    </ul><!-- .breadcrumbs -->
                </div>
            </div>
        </div>
        <!-- END: BREADCRUMBS -->
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <main class="col-xs-12 col-sm-12">
                    <div class="auth-form-wrapper">

                        <div class="auth-form">
                            <h1>Восстановление пароля</h1>

                            <form role="form" method="POST" class="form-horizontal" action="{{ route('password.request') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="token" value="{{ $token }}">

                                <label for="email">E-Mail</label>
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block has-error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                                <label for="password">Пароль</label>
                                <input id="password" type="password" class="form-control" name="password"
                                       required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                                <label for="password-confirm">Повторите пароль</label>
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                @endif

                                <div class="auth-form-footer">
                                    <div>
                                        <button type="submit" class="btn btn-primary">Сменить пароль</button>
                                    </div>
                                    <div class="text-right">
                                        <div><a href="{{route('login')}}">Войти</a></div>
                                        <div><a href="{{route('register')}}">Регистрация</a></div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </main>
            </div>
        </div>
    </section>
@endsection
