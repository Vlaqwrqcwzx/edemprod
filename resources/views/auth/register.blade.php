@extends('layouts.front.app')

@section('content')
    <section class="shop">
        <!-- START: BREADCRUMBS -->
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumbs">
                        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{ route('home') }}" itemprop="url">Главная</a></li>
                        <li class="sep">&gt;</li>
                        <li class="current">Регистрация</li>
                    </ul><!-- .breadcrumbs -->
                </div>
            </div>
        </div>
        <!-- END: BREADCRUMBS -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">@include('layouts.errors-and-messages')</div>
                <main class="col-xs-12 col-sm-12">
                    <form class="form-horizontal reg-form" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <h1>Регистрация</h1>

                        <div class="reg-form-row">
                            <div class="{{ $errors->has('name') ? 'has-error' : '' }}">
                                <label for="name">Имя*</label>
                                <input id="name" type="text" class="form-control" name="name"
                                       value="{{ session()->get('name') ?? old('name') }}" @if(empty(session()->get('email'))) autofocus @endif required>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="{{ $errors->has('password') ? 'has-error' : '' }} reg-pass-div">
                                <label for="password">Пароль*</label>
                                <div class="rich-input rich-input-password">
                                    <input id="password" type="password" class="rich-input-field" name="password"
                                           value="" required @if(!empty(session()->get('email'))) autofocus @endif>
                                    <img class="rich-input-addon"
                                         src="{{ asset('icons/see.svg') }}"
                                         alt="See">
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="reg-form-row">
                            <div class="{{ $errors->has('email') ? 'has-error' : '' }} reg-email-div">
                                <label for="email">E-mail*</label>
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ session()->get('email') ?? old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div>
                                <label for="password-confirm">Повторите пароль*</label>
                                <div class="rich-input rich-input-password">
                                    <input id="password-confirm" type="password" class="rich-input-field"
                                           name="password_confirmation" value="" required>
                                    <img class="rich-input-addon"
                                         src="{{ asset('icons/see.svg') }}"
                                         alt="See">
                                </div>
                            </div>
                        </div>

                        <div class="reg-form-row">
                            <div>
                                <label for="phone">Телефон</label>
                                <input type="text" name="phone" data-double="phone" class="form-control"
                                       data-mask="+7 (000) 000 00 00" placeholder="+7 (___) ___ __ __"
                                       autocomplete="off" maxlength="18" required
                                       value="{{ session()->get('phone') ?? old('phone') }}">
                            </div>

                            <div class="custom-checkbox">
                                <div class="checkbox-item">
                                    <input id="custom-checkbox" type="checkbox" required>
                                    <label for="custom-checkbox"></label>
                                </div>
                                <div><span>Я даю согласие на обработку своих</span><br>
                                    <a href="#">персональных данных</a></div>
                            </div>
                        </div>

                        <div class="reg-form-row align-items-center">
                            <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
                            <div><span>Есть аккаунт?</span> <a href="{{ route('login') }}">Выполните вход</a></div>
                        </div>

                    </form>
                </main>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('assets/js/jquery.mask.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('input[data-double="phone"]').mask('+0 (000) 000 00 00')
        })
    </script>
@stop