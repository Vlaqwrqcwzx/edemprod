@extends('layouts.front.app')

@section('content')
    <section class="shop">
        <!-- START: BREADCRUMBS -->
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumbs">
                        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a
                                    href="{{ route('home') }}" itemprop="url">Главная</a></li>
                        <li class="sep">&gt;</li>
                        <li class="current">Авторизация</li>
                    </ul><!-- .breadcrumbs -->    </div>
            </div>
        </div>
        <!-- END: BREADCRUMBS -->
        <div class="container">

            <div class="row">
                <div class="col-md-12">@include('layouts.errors-and-messages')</div>
                <main class="col-xs-12 col-sm-12">
                    <div class="auth-form-wrapper">

                        <div class="auth-form">
                            <h1>Авторизация</h1>

                            <form action="{{ route('login') }}" method="post" class="form-horizontal">
                                {{ csrf_field() }}
                                <label for="login">E-mail</label>
                                <input type="email" id="email" name="email" value="{{ session()->get('email') ?? old('email') }}"
                                       class="form-control"
                                       placeholder="Email" autofocus>
                                <label for="password">Пароль</label>
                                <div class="rich-input rich-input-password">
                                    <input class="rich-input-field" type="password" id="password" name="password">
                                    <img class="rich-input-addon"
                                         src="{{ asset('icons/see.svg') }}"
                                         alt="See">
                                </div>
                                <div class="auth-form-footer">
                                    <div>
                                        <button type="submit" class="btn btn-primary">Войти</button>
                                    </div>
                                    <div class="text-right">
                                        <div><a href="{{route('password.request')}}">Забыли пароль?</a></div>
                                        <div><a href="{{route('register')}}">Регистрация</a></div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </main>
            </div>
        </div>
    </section>
@endsection
