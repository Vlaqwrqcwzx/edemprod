@extends('layouts.admin.app')

@section('content')
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <div class="box-body">
                <h2><i class="fa fa-cloud-upload"></i> Импорт товаров</h2>
                <div class="row">
                    <div class="col-md-5">
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title"></h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carousel-example-generic" data-slide-to="0"
                                            class="active"></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <img src="{{ asset('img/sheets.png') }}"
                                                 alt="First slide">

                                            <div class="carousel-caption">
                                            </div>
                                        </div>
                                        <div class="item">
                                            <img src="{{ asset('img/parser1.png') }}"
                                                 alt="First slide">

                                            <div class="carousel-caption">
                                                Заполните Google таблицу согласно инструкции
                                            </div>
                                        </div>
                                        <div class="item">
                                            <img src="{{ asset('img/parser2.png') }}"
                                                 alt="Second slide">

                                            <div class="carousel-caption">
                                            </div>
                                        </div>
                                        <div class="item">
                                            <img src="{{ asset('img/parser3.png') }}"
                                                 alt="Third slide">

                                            <div class="carousel-caption">
                                                Скопируйте ID таблицы
                                            </div>
                                        </div>
                                        <div class="item">
                                            <img src="{{ asset('img/parser4.png') }}"
                                                 alt="Third slide">

                                            <div class="carousel-caption">
                                                Вставьте скопированное значение и нажмите импортировать!
                                            </div>
                                        </div>
                                    </div>
                                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                        <span class="fa fa-angle-left"></span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-example-generic"
                                       data-slide="next">
                                        <span class="fa fa-angle-right"></span>
                                    </a>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <div class="col-md-7">
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <i class="fa fa-comments"></i>
                                <h3 class="box-title">Инструкция</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <dl class="row">
                                    <dt class="col-md-3">Данные</dt>
                                    <dd class="col-md-9 text-left">
                                        Заполните поля продуктов строго представленному шаблону
                                    </dd>
                                    <dt class="col-md-3">Файл шаблона</dt>
                                    <dd class="col-md-9 text-left"><a target="_blank"
                                                href="https://docs.google.com/spreadsheets/d/1RaaMqRxPMwp_6QvDpWcaeWbPTZ0Tnv--q_xWWSXBJ-s/edit?usp=sharing">Ассортимент
                                            продуктов.exel</a></dd>
                                    <dt class="col-md-3 mt-3">Категория</dt>
                                    <dd class="col-md-9 text-left"><u>Название категории</u> - к которой будет привязан продукт, должна быть создана в системе
                                    </dd>
                                    <dt class="col-md-3 mt-3">Артикул</dt>
                                    <dd class="col-md-9 text-left"><u>Текстовое значение</u></dd>
                                    <dt class="col-md-3 mt-3">Название</dt>
                                    <dd class="col-md-9 text-left"><u>Текстовое значение</u></dd>
                                    <dt class="col-md-3 mt-3">Состав букета</dt>
                                    <dd class="col-md-9 text-left"><u>Текстовое значение</u></dd>
                                    <dt class="col-md-3 mt-3">Галерея изображений</dt>
                                    <dd class="col-md-9 text-left"><u>Ссылка на папку с изображениями</u>
                                    </dd>
                                    <dt class="col-md-3 mt-3">Количество</dt>
                                    <dd class="col-md-9 text-left"><u>Числовое значение</u></dd>
                                    <dt class="col-md-3 mt-3">Распродажа</dt>
                                    <dd class="col-md-9 text-left"><u>0 если нет, 1 если да</u> - 0 если товар не
                                        участвует в распродаже, 1 в противном
                                    </dd>
                                    <dt class="col-md-3 mt-3">Цена</dt>
                                    <dd class="col-md-9 text-left"><u>Числовое значение</u> - цена указывается только за
                                        стандартный размер M все остальные рассчитываются , как указано в настройках
                                        атрибутов
                                    </dd>
                                    <dt class="col-md-3 mt-3">Цена распродажи</dt>
                                    <dd class="col-md-9 text-left"><u>Числовое значение</u> - цена указывается только
                                        если товар участвует в акции
                                    </dd>
                                    <dt class="col-md-3 mt-3">Размеры</dt>
                                    <dd class="col-md-9 text-left"><u>M (30-40 см);L (45-50 см);XL (55-70 см)</u> - указывается большими буквами как в
                                        системе , через точку с запятой
                                    </dd>
                                    <dt class="col-md-3 mt-3">#Хештеги</dt>
                                    <dd class="col-md-9 text-left"><u>орхидея;букетсорхидеей;раскидистыйбукет</u> -
                                        указывается через точку с запятой, знак # ставить не нужно, система поставит все
                                        сама
                                    </dd>
                                    <dt class="col-md-3 mt-3">ID Таблицы</dt>
                                    <dd class="col-md-9 text-left">После того как вы запонили вашу таблицу, скопируйте ID вашей таблицы, вставьте в поле и нажмите ИМПОРТИРОВАТЬ</dd>
                                </dl>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <i class="fa fa-comments"></i>
                                <h3 class="box-title">Импорт</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <dl class="row">
                                    <dt class="col-md-3 mt-3">Безопасность</dt>
                                    <dd class="col-md-9 text-left">Ограничьте доступ к вашей иаблице</dd>
                                    <dt class="col-md-3 mt-3">Доступ для системы</dt>
                                    <dd class="col-md-9 text-left">Предоставьте доступ для пользователя <a href="javascript:void(0);">googlephp@virtual-flux-288611.iam.gserviceaccount.com</a> , чтобы сайт мог читать вашу таблицу</dd>
                                    <dt class="col-md-3 mt-3">ID Таблицы</dt>
                                    <dd class="col-md-9 text-left">После того как вы запонили вашу таблицу, скопируйте ID таблицы, вставьте в поле и нажмите ИМПОРТИРОВАТЬ</dd>
                                </dl>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="col-md-5">
                        <form action="{{ route('admin.import.execute') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="address">ID Таблицы</label>
                                <input type="text" class="form-control" id="address" name="address"
                                       placeholder="1RaaMqRxPMwp_6QvDasfQevbPTZ0Tnv--q_xWWSXBJ-s"
                                       value="1RaaMqRxPMwp_6QvDpWcaeWbPTZ0Tnv--q_xWWSXBJ-s"
                                >
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Импортировать</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('css')
    <style>
        .carousel-caption {
            color: #343a40 !important;
            font-size: 23px;
        }
    </style>
@endsection