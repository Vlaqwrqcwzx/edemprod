@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.categories.store') }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="parent">Родительская категория</label>
                        <select name="parent" id="parent" class="form-control select2">
                            <option value="">Нет родительской категории</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Название <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="Название" class="form-control" value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">Описание </label>
                        <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Описание">{{ old('description') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="cover">Изображение </label>
                        <input type="file" name="cover" id="cover" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="cover_icon">Иконка </label>
                        <input type="text" name="cover_icon" id="cover_icon" placeholder="fa-icon" class="form-control" value="{{ old('cover_icon') }}">
                    </div>
                    <div class="form-group">
                        <label for="status">Статус </label>
                        <select name="status" id="status" class="form-control">
                            <option value="0">Выкл.</option>
                            <option value="1">Вкл.</option>
                        </select>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.categories.index') }}" class="btn btn-default">Назад</a>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
