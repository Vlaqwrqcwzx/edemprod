@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.categories.update', $category->id) }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    <input type="hidden" name="_method" value="put">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="parent">Родительская категория</label>
                        <select name="parent" id="parent" class="form-control select2">
                            <option value="0">Нет родительской категории</option>
                            @foreach($categories as $cat)
                                <option @if($cat->id == $category->parent_id) selected="selected" @endif value="{{$cat->id}}">{{$cat->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Название <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="Название" class="form-control" value="{!! $category->name ?: old('name')  !!}">
                    </div>
                    <div class="form-group">
                        <label for="description">Описание </label>
                        <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Описание">{!! $category->description ?: old('description')  !!}</textarea>
                    </div>
                    @if(isset($category->cover))
                    <div class="form-group">
                        <p>Изображение</p>
                        <img src="{{ asset("storage/$category->cover") }}" alt="" class="img-responsive"> <br/>
                        {{--<a onclick="return confirm('Are you sure?')" href="{{ route('admin.category.remove.image', ['category' => $category->id]) }}" class="btn btn-danger">Remove image?</a>--}}
                    </div>
                    @endif
                    @if(isset($category->cover_icon))
                        <div class="form-group">
                            <label for="cover_icon">Иконка </label>
                            <input type="text" name="cover_icon" id="cover_icon" placeholder="fa-icon" class="form-control" value="{{ $category->cover_icon ?: old('cover_icon') }}">
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="cover">Изображение </label>
                        <input type="file" name="cover" id="cover" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="cover_icon">Иконка </label>
                        <input type="file" name="cover_icon" id="cover_icon" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="status">Статус </label>
                        <select name="status" id="status" class="form-control">
                            <option value="0" @if($category->status == 0) selected="selected" @endif>Выкл.</option>
                            <option value="1" @if($category->status == 1) selected="selected" @endif>Вкл.</option>
                        </select>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.categories.index') }}" class="btn btn-default">Назад</a>
                        <button type="submit" class="btn btn-primary">Обновить</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
