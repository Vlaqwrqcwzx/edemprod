@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="row">
            <div class="col-md-3 margin-bottom">
                <a href="{{ route('admin.coupons.create') }}" class="btn btn-block btn-success">Добавить купон</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <table class="table">
                        <thead>
                        <tr>
                            <td>Код</td>
                            <td>Начало действия</td>
                            <td>Конец дейсвия</td>
                            <td>Количество</td>
                            <td>Описание</td>
                            <td>Процент скидки</td>
                            <td>Действия</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($coupons as $coupon)
                            <tr>
                                <td>{{ $coupon->code }}</td>
                                <td>{{ $coupon->startDate }}</td>
                                <td>{{ $coupon->endDate }}</td>
                                <td>{{ $coupon->count }}</td>
                                <td>{{ $coupon->caption }}</td>
                                <td>{{ $coupon->sale_percent }}</td>
                                <td>
                                    <form action="{{ route('admin.coupons.destroy', $coupon) }}" method="post" accept-charset="UTF-8" class="form-horizontal">
                                        @csrf
                                        <input type="hidden" name="_method" value="delete"/>
                                        <div class="btn-group">
                                            <a href="{{ route('admin.coupons.edit', $coupon) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Редактировать</a>
                                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Удалить</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
