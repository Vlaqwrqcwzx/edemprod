@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="col-md-6">
            <div class="box">
                <form action="{{ route('admin.coupons.update', $coupon) }}" method="post" class="form"
                      enctype="multipart/form-data">
                    <div class="box-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">
                        <div class="form-group">
                            <label for="code">Код <span class="text-danger">*</span></label>
                            <input type="text" name="code" id="code" placeholder="Код" class="form-control" value="{!! $coupon->code ?: old('code')  !!}" required>
                        </div>
                        <div class="form-group">
                            <label for="startDate">Начало действия <span class="text-danger">*</span></label>
                            <input type="date" name="startDate" id="startDate" placeholder="Начало действия"
                                   class="form-control" value="{{ Carbon\Carbon::parse($coupon->startDate)->format('Y-m-d') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="endDate">Конец дейсвия <span class="text-danger">*</span></label>
                            <input type="date" name="endDate" id="endDate" placeholder="Конец дейсвия" class="form-control"
                                   value="{{ Carbon\Carbon::parse($coupon->endDate)->format('Y-m-d') }}"
                                   required>
                        </div>
                        <div class="form-group">
                            <label for="count">Количество <span class="text-danger">*</span></label>
                            <input type="number" name="count" id="count" min="0" class="form-control" value="{!! $coupon->count ?: old('count')  !!}" required>
                        </div>
                        <div class="form-group">
                            <label for="caption">Описание <span class="text-danger">*</span></label>
                            <input type="text" name="caption" id="caption" class="form-control" value="{!! $coupon->caption ?: old('caption')  !!}" required>
                        </div>
                        <div class="form-group">
                            <label for="sale_percent">Процент скидки <span class="text-danger">*</span></label>
                            <input type="number" name="sale_percent" id="sale_percent" min="0"  max="100" value="{!! $coupon->sale_percent ?: old('sale_percent')  !!}" class="form-control" required>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="btn-group">
                            <a href="{{ route('admin.coupons.index') }}" class="btn btn-default">Назад</a>
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
