@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($pages)
            <div class="box">
                <div class="box-body">
                    <h2>Страницы</h2>
                    <table class="table">
                        <thead>
                        <tr>
                            <td>Ссылка</td>
                            <td>Название</td>
                            <td>Действие</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($pages as $page)
                            <tr>
                                <td>
                                    {{ $page->slug }}
                                </td>
                                <td>
                                    {!! $page->title !!}
                                </td>
                                <td>
                                    <a href="{{ route('admin.pages.edit', $page) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Редактировать</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box -->
        @endif

    </section>
    <!-- /.content -->
@endsection