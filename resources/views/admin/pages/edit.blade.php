@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.pages.update', $page) }}" method="post" class="form">
                <div class="box-body">
                    {{ csrf_field() }}
                    <input type="hidden" value="put" name="_method">
                    <div class="form-group">
                        <label for="title">Название <span class="text text-danger">*</span></label>
                        <input type="text" name="title" id="title" placeholder="Название" class="form-control" value="{{ old('title') ?: $page->title }}" required>
                    </div>
                    <div class="form-group">
                        <label for="text">Описание</label>
                        <textarea name="text" id="text" class="form-control ckeditor" placeholder="text"> {!! old('text') ?: $page->text !!}</textarea>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <div class="btn-group">
                            <a href="{{ route('admin.pages.index') }}" class="btn btn-default">Назад</a>
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
