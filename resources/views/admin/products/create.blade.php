@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.products.store') }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    {{ csrf_field() }}
                    <div class="col-md-8">
                        <h2>Продукт</h2>
                        <div class="form-group">
                            <label for="sku">Артикул <span class="text-danger">*</span></label>
                            <input type="text" name="sku" id="sku" placeholder="xxxxx" class="form-control"
                                   value="{{ old('sku') }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Название <span class="text-danger">*</span></label>
                            <input type="text" name="name" id="name" placeholder="Название" class="form-control"
                                   value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <label for="description">Описание </label>
                            <textarea class="form-control" name="description" id="description" rows="5"
                                      placeholder="Описание">{{ old('description') }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="cover">Изображение </label>
                            <input type="file" name="cover" id="cover" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="cover_hover">Изображение при наведении</label>
                            <input type="file" name="cover_hover" id="cover_hover" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="image">Галерея</label>
                            <input type="file" name="image[]" id="image" class="form-control" multiple>
                            <small class="text-warning">Вы можете использовать ctr (cmd) для выбора нескольких изображений</small>
                        </div>
                        <div class="form-group">
                            <label for="quantity">Количество <span class="text-danger">*</span></label>
                            <input type="text" name="quantity" id="quantity" placeholder="10" class="form-control"
                                   value="{{ old('quantity') }}">
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="1"
                                       id="is_sale" name="is_sale">
                                <label class="form-check-label" for="is_sale">
                                    Участвует в распродаже
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price">Цена <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <span class="input-group-addon">PHP</span>
                                <input type="text" name="price" id="price" placeholder="Price" class="form-control"
                                       value="{{ old('price') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sale_price">Цена со скидкой (если товар учавствует в распродаже)<span class="text-danger">*</span></label>
                            <div class="input-group">
                                <span class="input-group-addon">PHP</span>
                                <input type="text" name="sale_price" id="sale_price" placeholder="Sale Price"
                                       class="form-control" value="{{ old('sale_price') }}">
                            </div>
                        </div>
                        @if(!$brands->isEmpty())
                            <div class="form-group">
                                <label for="brand_id">Брэнд </label>
                                <select name="brand_id" id="brand_id" class="form-control select2">
                                    <option value=""></option>
                                    @foreach($brands as $brand)
                                        <option @if(old('brand_id') == $brand->id) selected="selected"
                                                @endif value="{{ $brand->id }}">{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="form-group">
                            <ul class="list-unstyled attribute-lists">
                                @foreach($attributes as $attribute)
                                    <li>
                                        <label for="attribute{{ $attribute->id }}" class="checkbox-inline">
                                            {{ $attribute->name }}
                                            <input name="attribute[]" class="attribute" type="checkbox"
                                                   id="attribute{{ $attribute->id }}" value="{{ $attribute->id }}">
                                        </label>

                                        <label for="attributeValue{{ $attribute->id }}"
                                               style="display: none; visibility: hidden"></label>
                                        @if(!$attribute->values->isEmpty())
                                            <select multiple name="attributeValue[]"
                                                    id="attributeValue{{ $attribute->id }}" class="form-control select2"
                                                    style="width: 100%" disabled>
                                                @foreach($attribute->values as $attr)
                                                    <option value="{{ $attr->id }}">{{ $attr->name }}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        @include('admin.shared.status-select', ['status' => 0])
                    </div>
                    <div class="col-md-4">
                        <h2>Категории</h2>
                        @include('admin.shared.categories', ['categories' => $categories, 'selectedIds' => []])
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.products.index') }}" class="btn btn-default">Назад</a>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@section('css')
    <style type="text/css">
        label.checkbox-inline {
            padding: 10px 5px;
            display: block;
            margin-bottom: 5px;
        }

        label.checkbox-inline > input[type="checkbox"] {
            margin-left: 10px;
        }

        ul.attribute-lists > li > label:hover {
            background: #3c8dbc;
            color: #fff;
        }

        ul.attribute-lists > li {
            background: #eee;
        }

        ul.attribute-lists > li:hover {
            background: #ccc;
        }

        ul.attribute-lists > li {
            margin-bottom: 15px;
            padding: 15px;
        }
    </style>
@endsection
@section('js')
    <script type="text/javascript">
        function backToInfoTab() {
            $('#tablist > li:first-child').addClass('active');
            $('#tablist > li:last-child').removeClass('active');

            $('#tabcontent > div:first-child').addClass('active');
            $('#tabcontent > div:last-child').removeClass('active');
        }
        $(document).ready(function () {
            const checkbox = $('input.attribute');
            $(checkbox).on('change', function () {
                const attributeId = $(this).val();
                if ($(this).is(':checked')) {
                    $('#attributeValue' + attributeId).attr('disabled', false);
                } else {
                    $('#attributeValue' + attributeId).attr('disabled', true);
                }
                const count = checkbox.filter(':checked').length;
                if (count > 0) {
                    $('#productAttributeQuantity').attr('disabled', false);
                    $('#productAttributePrice').attr('disabled', false);
                    $('#salePrice').attr('disabled', false);
                    $('#default').attr('disabled', false);
                    $('#createCombinationBtn').attr('disabled', false);
                    $('#combination').attr('disabled', false);
                } else {
                    $('#productAttributeQuantity').attr('disabled', true);
                    $('#productAttributePrice').attr('disabled', true);
                    $('#salePrice').attr('disabled', true);
                    $('#default').attr('disabled', true);
                    $('#createCombinationBtn').attr('disabled', true);
                    $('#combination').attr('disabled', true);
                }
            });
        });
    </script>
@endsection
