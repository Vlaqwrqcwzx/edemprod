@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <table class="table">
                        <thead>
                        <tr>
                            <td>Название</td>
                            <td>Телефон</td>
                            <td>Адрес</td>
                            <td>Действия</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cities as $city)
                            <tr>
                                <td>{{ $city->name }}</td>
                                <td>{{ $city->phone }}</td>
                                <td>{{ $city->address }}</td>
                                <td>
                                    <form action="{{ route('admin.cities.destroy', $city) }}" method="post" accept-charset="UTF-8" class="form-horizontal">
                                        @csrf
                                        <input type="hidden" name="_method" value="delete"/>
                                        <div class="btn-group">
                                            <a href="{{ route('admin.cities.edit', $city) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Редактировать</a>
                                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Удалить</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
