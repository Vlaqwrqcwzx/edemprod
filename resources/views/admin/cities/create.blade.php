@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.cities.store') }}" method="post" class="form"
                  enctype="multipart/form-data">
                <div class="box-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Название <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="Название" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="phone">Телефон <span class="text-danger">*</span></label>
                        <input type="text" name="phone" id="phone" placeholder="Телефон" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="address">Адрес <span class="text-danger">*</span></label>
                        <input type="text" name="address" id="address" placeholder="Адрес" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="map_href">Ссылка для карты <span class="text-danger">*</span></label>
                        <input type="text" name="map_href" id="map_href" placeholder="Ссылка для карты" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="map">Карта <span class="text-danger">*</span></label>
                        <input type="text" name="map" id="map" placeholder="Карта" class="form-control">
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.cities.index') }}" class="btn btn-default">Назад</a>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
