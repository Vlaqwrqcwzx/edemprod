@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.brands.update', $brand->id) }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="put">
                    <div class="form-group">
                        <label for="name">Название <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="Название" class="form-control" value="{{ $brand->name }}">
                    </div>
                    <div class="form-group">
                        <label for="cover_icon">Иконка</label>
                        <input type="text" name="cover_icon" id="cover_icon" placeholder="ico-h-promo" class="form-control" value="{{ $brand->cover_icon }}">
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.brands.index') }}" class="btn btn-default">Назад</a>
                        <button type="submit" class="btn btn-primary">Обновить</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
