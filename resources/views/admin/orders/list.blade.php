@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($orders)
            <div class="box">
                <div class="box-body">
                    <h2>Заказы</h2>
                    @include('layouts.search', ['route' => route('admin.orders.index')])
                    <table class="table">
                        <thead>
                            <tr>
                                <td class="col-md-3">Дата заказа</td>
                                <td class="col-md-3">Клиент</td>
                                <td class="col-md-2">Доставка</td>
                                <td class="col-md-2">Сумма</td>
                                <td class="col-md-2">Статус</td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td><a title="Показать заказ" href="{{ route('admin.orders.show', $order->id) }}">{{ Carbon\Carbon::parse($order->created_at)->format('d.m.Y H:i')}} ({{ Carbon\Carbon::parse($order->created_at)->diffForHumans() }})</a></td>
                                <td>{{ $order->customer->name }}</td>
                                <td>{{ $order->courier->name }}</td>
                                <td>
                                    <span class="label @if($order->status->name != 'paid') label-danger @else label-success @endif">{{ $order->total }} {{ config('cart.currency') }}</span>
                                </td>
                                <td><p class="text-center" style="color: #ffffff; background-color: {{ $order->status->color }}">{{ $order->status->display_name }}</p></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ $orders->links() }}
                </div>
            </div>
            <!-- /.box -->
        @endif

    </section>
    <!-- /.content -->
@endsection