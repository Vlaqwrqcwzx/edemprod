@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <h2>
                            <small>Клиент: </small>
                            <a href="{{ route('admin.customers.show', $order->customer->id) }}">{{$order->customer->name}}</a> <br />
                            <small>Email: {{$order->customer->email}}</small> <br />
                            <small>reference: <strong>{{$order->reference}}</strong></small>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <h4> <i class="fa fa-shopping-bag"></i> Информация о заказе</h4>
                <table class="table">
                    <thead>
                    <tr>
                        <td class="col-md-3">Дата и время доставки</td>
                        <td class="col-md-3">Клиент</td>
                        <td class="col-md-3">Оплата</td>
                        <td class="col-md-3">Статус заказа</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ Carbon\Carbon::parse($order['shipping_date'])->format('d.m.Y')}} {{ $order['shipping_time'] }}</td>
                        <td><a href="{{ route('admin.customers.show', $order->customer->id) }}">{{ $order->customer->name }}</a></td>
                        <td><strong>{{ $order->payment }}</strong></td>
                        <td>
                            @foreach($statuses as $status)
                                @if($order->status->id == $status->id)
                                    <p>{{ $status->name }}</p>
                                @endif
                            @endforeach
                        </td>
                    </tr>
                    </tbody>
                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">Товаров на сумму</td>
                        <td class="bg-warning">{{round($order['total_products'])}} {{ config('cart.currency_symbol') }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="bg-info">Доставка:</td>
                        <td class="bg-info">{{ $order['courier']['name'] }}, {{round($order['total_shipping'])}} {{ config('cart.currency_symbol') }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">Открытки:</td>
                        <td class="bg-warning">{{round($order['total_postcards'])}} {{ config('cart.currency_symbol') }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="bg-danger">Скидка:</td>
                        <td class="bg-danger">{{round($order['discounts'])}} {{ config('cart.currency_symbol') }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="bg-success text-bold">К оплате:</td>
                        <td class="bg-success text-bold">{{round($order['total'])}} {{ config('cart.currency_symbol') }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        @if($order)
            <div class="box">
                @if(!empty($order['products']))
                    <div class="box-body">
                        <h4> <i class="fa fa-gift"></i> Продукты</h4>
                        <table class="table">
                            <thead>
                            <th class="col-md-2">Изображение</th>
                            <th class="col-md-2">Название</th>
                            <th class="col-md-1">Артикул</th>
                            <th class="col-md-2">Размер букета</th>
                            <th class="col-md-2">Открытка</th>
                            <th class="col-md-1">Цена за шт.</th>
                            <th class="col-md-1">Количество</th>
                            <th class="col-md-1">Итого</th>
                            </thead>
                            <tbody>
                            @foreach ($order['products'] as $product)
                                <tr>
                                    <td>
                                        <img height="100" src="{{ asset("storage/".$product['cover']) }}"
                                             alt="{{ $product['name'] }}">
                                    </td>
                                    <td>{{ $product['name'] }}</td>
                                    <td>{{ $product['sku'] }}</td>
                                    <td>{{ \App\Shop\AttributeValues\AttributeValue::find($product['pivot']['product_attribute_id'])->name }}</td>
                                    <td>
                                        @if($product['pivot']['use_postcard'] == 'use')
                                            <p>В букет добавлена открытка с пожеланиями</p>
                                            <p>Текст: {!! $product['pivot']['postcard_text'] ?? '' !!}</p>
                                        @endif
                                    </td>
                                    <td>{{ round($product['pivot']['product_price']) }} {{ config('cart.currency_symbol') }}</td>
                                    <td>{{$product['pivot']['quantity']}}</td>
                                    <td>{{ round($product['pivot']['product_price']*$product['pivot']['quantity']) }} {{ config('cart.currency_symbol') }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4> <i class="fa fa-truck"></i> Доставка</h4>
                            <table class="table">
                                <thead>
                                <th class="col-md-3">Название</th>
                                <th class="col-md-4">Описание</th>
                                <th class="col-md-5">Ссылка</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $order->courier->name }}</td>
                                    <td>{{ $order->courier->description }}</td>
                                    <td>{{ $order->courier->url }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <h4> <i class="fa fa-map-marker"></i> Адрес</h4>
                            <table class="table">
                                <thead>
                                <th>Город</th>
                                <th>Адрес</th>
                                <th>Дом</th>
                                <th>Корпус</th>
                                <th>Квартира</th>
                                <th>Домофон</th>
                                <th>Офис</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $order['city']['name'] }}</td>
                                    <td>{{ $order['address']['billing_address'] }}</td>
                                    <td>{{ $order['address']['house'] }}</td>
                                    <td>{{ $order['address']['corpus'] }}</td>
                                    <td>{{ $order['address']['flat'] }}</td>
                                    <td>{{ $order['address']['domofon'] }}</td>
                                    <td>{{ $order['address']['office'] }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6 mb-3 mt-3">
                            <h4> <i class="fa fa-comment"></i> Комментарий к заказу</h4>
                            <p>{{ $order->comment }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <div class="box-footer">
                <div class="btn-group">
                    <a href="{{ route('admin.orders.index') }}" class="btn btn-default">Назад</a>
                    @if($user->hasPermission('update-order'))
                        <a href="{{ route('admin.orders.edit', $order->id) }}" class="btn btn-primary">Редактировать</a>
                    @endif
                </div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection