<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Invoice</title>
    <link rel="stylesheet"
          href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css')}}">
    <style type="text/css">
        table {
            border-collapse: collapse;
        }
    </style>
</head>
<body>
<section class="container">
    <div class="col-md-12">
        <h2>Здраствуйте {{$order->customer->name}}!</h2>
        <p>Заказ #{{ $order['id'] }} от {{ Carbon\Carbon::parse($order['created_at'])->format('d.m.yy H:i') }}</p>
        <p>Информация о заказе</p>
        <p>Дата и время доставки:
            <strong>{{ Carbon\Carbon::parse($order['shipping_date'])->format('d.m.Y')}} {{ $order['shipping_time'] }}</strong>
        </p>
        <p>Доставка: {{ $order['courier']['name'] }}, {{round($order['total_shipping'])}} {{ config('cart.currency_symbol') }}</p>
        <p>Доставка по адресу: <strong>{{ $order['address']['city']['name'] }}, {{ $order['address']['billing_address'] }}
                , {{ $order['address']['house'] }}, {{ $order['address']['corpus'] }}
                , {{ $order['address']['flat'] }}, {{ $order['address']['domofon'] }}
                , {{ $order['address']['office'] }}</strong></p>
        <p>Оплачено: <strong>{{round($order['total'])}} {{ config('cart.currency_symbol') }}</strong></p>

        <p>Содержимое заказа</p>
        @foreach ($order['products'] as $product)
            <p>{{ $product['name'] }}</p>
            <p>Артикул: {{ $product['sku'] }}</p>
            <p>Размер букета: {{ \App\Shop\AttributeValues\AttributeValue::find($product['pivot']['product_attribute_id'])->name }}</p>
            <p>{{ $product['pivot']['use_postcard'] == 'use' ? 'В букет добавлена открытка с пожеланиями' : ''}}</p>
            <p>Количество: {{$product['pivot']['quantity']}}</p>
            <p>Итого: {{ round($product['pivot']['product_price']*$product['pivot']['quantity']) }} {{ config('cart.currency_symbol') }}</p>
        @endforeach
    </div>
</section>
</body>
</html>