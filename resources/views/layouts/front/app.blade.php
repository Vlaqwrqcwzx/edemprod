<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Эдемский сад &#8212; Сайт модной флористики</title>

    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/core.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">


    <link rel="icon" href="{{ asset('favicon-150x150.png') }}"
          sizes="32x32"/>
    <link rel="icon" href="{{ asset('favicon.png') }}" sizes="192x192"/>
    <link rel="apple-touch-icon" href="{{ asset('favicon.png') }}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.min.css') }}"/>
    @yield('og')
    @yield('css')
</head>
<body class="home page-template-default page page-id-43">
<!-- START: WRAPPER -->
<div class="wrapper" id="wrapper">
    <!-- START: MOBILE NAV -->
    <nav class="mobile-nav">

        <!-- START: CART -->
        <a href="{{ route('cart.index') }}" class="mobile-cart-link">
            <span class="mobile-cart">
                <span id="mobile-minicart-qty" class="mobile-cart-qty">{{ $cartCount }}</span>
            </span>
        </a>

        <!-- END: CART -->

        <!-- START: LOGO -->
        <a href="{{ route('home') }}" class="mobile-logo"></a>
        <!-- END: LOGO -->

        <!-- START: MOBILE NAV TOGGLER -->
        <span class="mobile-nav-toggler" id="mobile-nav-toggler">
				<span class="mobile-nav-toggler__bar"></span>
				<span class="mobile-nav-toggler__bar"></span>
				<span class="mobile-nav-toggler__bar"></span>
			</span>
        <!-- END: MOBILE NAV TOGGLER -->


        <!-- START: MOBILE NAV BODY -->
        <div class="mobile-nav__body">

            <div class="mobile-nav-top">
                <span class="header-nav-top__item location expandable ml-0" id="location">
                    <span>{{ session()->has('city') ? session()->get('city.name') : $defaulCity->name }}</span>
                    <ul class="expandable__list">
                        @foreach($cities as $city)
                            <a href="{{ route('change-city', $city) }}"><li>{{ $city->name }}</li></a>
                        @endforeach
                    </ul>
                </span>
                <a class="header-nav-top__item phone"
                   href="tel:{{ session()->has('city') ? preg_replace("/[^0-9]/", '', session()->get('city.phone')) : preg_replace("/[^0-9]/", '', $defaulCity->phone) }}">
                    <span>{{ session()->has('city') ? session()->get('city.phone') : $defaulCity->phone }}</span>
                </a>
            </div>


            <!-- START: HEADER-BOTTOM-NAV -->
            <div class="header-nav-bottom mobile-nav-bottom">
                <ul id="menu-glavnoe-menyu" class="list-unstyled">
                    @foreach($categoriesBlade as $category)
                        <li id="menu-item-{{ $category->id }}"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-53">
                            <a href="{{ route('front.category.slug', $category->slug) }}"><i
                                        class="{{ $category->cover_icon }}"></i><span>{{ $category->name }}</span></a>
                        </li>
                    @endforeach
                        @php($cityName = session()->has('city') ? session()->get('city.name') : $defaulCity->name)
                        <li id="menu-item-62" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-62">
                            @if(($cityName == 'Москва'))
                                <a href="{{ route('pages.show', 'master-klassy') }}"><i class="icon-photostudia"></i> <span>Мастер классы</span></a>
                            @else
                                <a href="{{ route('pages.show', 'fotostudiya') }}"><i class="icon-photostudia"></i> <span>Фотостудия</span></a>
                            @endif
                    </li>
                    <li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63">
                        <a href="{{ route('pages.show', 'kontakty') }}"><i class="icon-about"></i>
                            <span>Контакты</span></a></li>
                </ul>
            </div>
            <!-- END: HEADER-BOTTOM-NAV -->


        </div>
        <!-- END: MOBILE NAV BODY -->
    </nav>
    <!-- END: MOBILE NAV -->
    <!-- START: HEADER -->
    <header class="header hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-xs-2">
                    <a href="/" class="logo">
                        <img width="200" height="126"
                             src="{{ asset('img/logo.svg') }}"
                             alt="Эдемский Сад - центр модной флористики">
                    </a>
                </div>
                <div class="col-xs-10">
                    <div class="row">
                        <!-- START: HEADER-TOP-NAV -->
                        <nav class="header-nav-top text-right">
                            <ul class="list-unstyled">
                                <li>
                                    <a class="header-nav-top__item phone"
                                       href="tel:{{ session()->has('city') ? preg_replace("/[^0-9]/", '', session()->get('city.phone')) : preg_replace("/[^0-9]/", '', $defaulCity->phone) }}">
                                        <span>{{ session()->has('city') ? session()->get('city.phone') : $defaulCity->phone }}</span>
                                    </a>
                                </li>
                                <li>
										<span class="header-nav-top__item location expandable" id="location">
											<span>{{ session()->has('city') ? session()->get('city.name') : $defaulCity->name }}</span>
											<ul class="expandable__list">
												@foreach($cities as $city)
                                                    <a href="{{ route('change-city', $city) }}"><li>{{ $city->name }}</li></a>
                                                @endforeach
											</ul>
										</span>
                                </li>
                                @if(auth()->check())
                                    <li>
                                        <span id="login" class="header-nav-top__item login">
                                            <a href="{{ route('accounts', ['tab' => 'current']) }}">{{ auth()->user()->name }}</a>
                                        </span>
                                    </li>
                                    <li>
                                        <span id="login" class="header-nav-top__item input">
                                            <a href="{{ route('logout') }}">Выйти</a>
                                        </span>
                                    </li>
                                @else
                                    <li>
                                        <span id="login" class="header-nav-top__item login">
                                            <a href="{{ route('login') }}">Войти</a>
                                        </span>
                                    </li>
                                @endif
                                <li>
                                    <span id="minicart" class="header-nav-top__item minicart">
                                        @if($cartCount == 0)
                                            <a class="cart-link" href="{{ route('cart.index') }}">
                                                Корзина пуста
                                            </a>
                                        @else
                                            <span class="inner text-left">
                                                В корзине <span class="minicart__totals">{{ $cartCount }}</span> товара: <span
                                                        class="minicart__price">{{ round($cartSum) }} р.</span><br>
                                                <a href="{{ route('cart.index') }}"
                                                   class="minicart__cta link--arrowed"><span>Оформить заказ</span></a>
                                            </span>
                                        @endif
                                    </span>
                                </li>
                            </ul>
                        </nav>
                        <!-- END: HEADER-TOP-NAV -->
                    </div>
                    <div class="row">
                        <!-- START: HEADER-BOTTOM-NAV -->
                        <nav class="header-nav-bottom text-right">
                            <ul id="menu-glavnoe-menyu-1" class="list-unstyled">

                                @foreach($categoriesBlade as $category)
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-{{ $category->id }}">
                                        <a
                                                href="{{ route('front.category.slug', $category->slug) }}"><i
                                                    class="{{ $category->cover_icon }}"></i><span>{{ $category->name }}</span></a>
                                    </li>
                                @endforeach
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-62">
                                    @if(($cityName == 'Москва'))
                                        <a href="{{ route('pages.show', 'master-klassy') }}"><i class="icon-photostudia"></i> <span>Мастер классы</span></a>
                                    @else
                                        <a href="{{ route('pages.show', 'fotostudiya') }}"><i class="icon-photostudia"></i> <span>Фотостудия</span></a>
                                    @endif
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63"><a
                                            href="{{ route('pages.show', 'kontakty') }}"><i class="icon-about"></i>
                                        <span>Контакты</span></a>
                                </li>
                            </ul>
                        </nav>
                        <!-- END: HEADER-BOTTOM-NAV -->
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- END: HEADER -->

@yield('content')



<!-- START: FOOTER -->
    <footer class="hidden-xs">
        <div class="footer-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xs-4">
                        <a href="/" class="footer-logo">
                            <img src="{{ asset('img/logo_light.svg') }}"
                                 alt="Эдемский сад - центр модной флористики" width="200" height="126">
                        </a>
                        <span class="copy">
						© Центр модной флористики Эдемский Сад, <br/>2006 - 2020. Все права защищены
					</span>
                        <!-- START: FOOTER SOCIAL -->
                        <span class="socials">
						<a href="#">
							<i class="icon-fb"></i>
						</a>
						<a href="#">
							<i class="icon-vk"></i>
						</a>
						<a href="#">
							<i class="icon-insta"></i>
						</a>
					</span>
                        <!-- END: FOOTER SOCIAL -->
                    </div>
                    <div class="col-xs-3">
                        <!-- START: FOOTER NAV -->
                        <span class="footer-heading">Эдемский Сад</span>
                        <ul id="menu-menyu-v-podvale-1" class="list-unstyled footer-nav">
                            <li id="menu-item-65"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-65"><a
                                        href="{{ route('pages.show', 'o-kompanii') }}">О компании</a></li>
                            <li id="menu-item-117"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-117"><a
                                        href="{{ route('delivery') }}">Доставка и оплата</a>
                            </li>
                            <li id="menu-item-66"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-66"><a
                                        href="{{ route('pages.show', 'nashi-dostizheniya') }}">Наши достижения</a></li>
                            <li id="menu-item-67"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-67"><a
                                        href="{{ route('pages.show', 'smi-o-nas') }}">СМИ о нас</a></li>
                            <li id="menu-item-68"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-68"><a
                                        href="{{ route('pages.show', 'nashi-nagrady') }}">Наши награды</a></li>
                            <li id="menu-item-69"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-69"><a
                                        href="{{ route('pages.show', 'nashi-partnery-i-klienty') }}">Наши партнеры и клиенты</a></li>
                            <li id="menu-item-70"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-70"><a
                                        href="{{ route('pages.show', 'interer') }}">Интерьер</a></li>
                        </ul>
                        <!-- END: FOOTER NAV -->
                    </div>

                    <div class="col-xs-1 col-xs-offset-1 col-no-padding">
                        <!-- START: FOOTER NAV -->
                        @if(isset($cat1))
                            <span class="footer-heading">{{ $cat1->name }}</span>
                            <ul class="list-unstyled footer-nav">
                                @foreach($cat1->subcategories as $sub)
                                    <li><a href="{{ route('front.category.slug', $sub->slug) }}">{{ $sub->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                    @endif
                    <!-- END: FOOTER NAV -->
                    </div>
                    <div class="col-xs-2 col-xs-offset-1 col-pr-0">
                        <!-- START: FOOTER NAV -->
                        @if(isset($cat2))
                            <span class="footer-heading">{{ $cat2->name }}</span>
                            <ul class="list-unstyled footer-nav">
                                @foreach($cat2->subcategories as $sub)
                                    <li><a href="{{ route('front.category.slug', $sub->slug) }}">{{ $sub->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                    @endif
                    <!-- END: FOOTER NAV -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- END: FOOTER -->
    <!-- START: FOOTER MOBILE NAV -->
    <nav class="footer-mobile-nav">

        <a href="{{ route('home') }}">
            <span class="footer-mobile-nav__home"></span>
        </a>

        <a href="{{ route('accounts', ['tab' => 'current']) }}">
            <span class="footer-mobile-nav__login">
                <i class="icon-lk"></i>
            </span>
        </a>

    </nav>
    <!-- END: FOOTER MOBILE NAV -->
</div>
<!-- END: WRAPPER -->
<script src="{{ asset('assets/js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('assets/js/swiper.min.js') }}"></script>
<script src="{{ asset('assets/js/lightbox.js') }}"></script>
<script src="{{ asset('assets/js/slider.js') }}"></script>
<script src="{{ asset('assets/js/img_slider.js') }}"></script>
{{--<script src="{{ asset('assets/js/bootstrap.js') }}"></script>--}}
<script src="{{ asset('assets/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/js/datepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/toastr.min.js') }}"></script>
<script src="{{ asset('assets/js/index.js') }}"></script>
<script src="{{ asset('assets/js/jquery.fancybox.min.js') }}"></script>
@yield('js')
</body>
</html>
