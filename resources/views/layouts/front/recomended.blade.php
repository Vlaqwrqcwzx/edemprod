<div class="related-wrapper">
    <div class="related-title">Рекомендуем также</div>
    <div class="slider">
        <div class="slider__wrapper">
            @if(!empty($recomended))
                @foreach($recomended as $product)
                    <div class="slider__item">
                        <div class="related-item">
                            <div class="related-img-block">
                                @if(isset($product->cover))
                                    <a href="{{ route('front.get.product', str_slug($product->slug)) }}">
                                        <img src="{{ asset("storage/$product->cover") }}" alt="{{ $product->name }}">
                                    </a>
                                @else
                                    <img class="img-responsive img-thumbnail"
                                         src="{{ asset("https://placehold.it/180x180") }}"
                                         alt="{{ $product->name }}"/>
                                @endif
                                <div class="related-featured {{ $product->isFeatured() }}" data-toggle="featured" data-id="{{ $product->id }}"></div>
                                    @if($product->is_sale == 1)
                                        <span class="badge" title="Акция"> {{ 100 - round($product->sale_price*100/$product->price) }}%</span>
                                    @endif

                                <div class="related-img-tags">
                                    @foreach($product->attributesValues as $attr)
                                        @if($attr->attribute->name == 'Hashtags')
                                            <a href="javascript:void(0);" data-type="tag">{{ $attr->name }}</a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>

                            <div class="flex-row related-item-footer">
                                <div class="related-item-title">{{ $product->name }}</div>
                                <div class="related-price-block">
                                    <span class="product__price">
                                    @if($product->is_sale == 1)
                                            <span class="related-price-old">{{ round($product->price) }} р.</span>
                                            <span class="related-price">{{ round($product->sale_price) }} р.</span>
                                    @else
                                        <span class="related-price">{{ round($product->price) }} р.</span>
                                    @endif
                                    </span>


                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif


        </div>

        <div class="slider__controls">
            <a class="slider__control slider__control_left slider__control_left-inactive" href="#" role="button"></a>
            <a class="slider__control slider__control_right slider__control_right-active" href="#" role="button"></a>
        </div>
        <div class="slider__indicators">
            <div class="slider__indicator-active"></div>
            <div class="slider__indicator"></div>
            <div class="slider__indicator"></div>
            <div class="slider__indicator"></div>
        </div>
    </div>
</div>