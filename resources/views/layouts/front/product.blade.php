<div class="col-xs-12 col-sm-4 product">
    <div class="product__body">
        <div class="product__images">
            @if($product->is_sale == 1)
                <span class="badge" title="Акция"> {{ 100 - round($product->sale_price*100/$product->price) }}
                    %</span>
            @endif
            <span data-toggle="featured" data-id="{{ $product->id }}" class="{{ $product->isFeatured() }}"></span>
            <a href="{{ route('front.get.product', str_slug($product->slug)) }}">
                <img src="{{ asset("storage/$product->cover") }}" alt="{{ $product->name }}"
                     class="img-responsive product__top-image">
                <img src="{{ asset("storage/$product->cover_hover") }}"
                     alt="{{ $product->name }}" class="img-responsive product__bot-image">
            </a>
            <span class="product__tags">
                @foreach($product->attributesValues as $attr)
                    @if($attr->attribute->name == 'Hashtags')
                        <a href="javascript:void(0);" data-type="tag">{{ $attr->name }}</a>
                    @endif
                @endforeach
            </span>

        </div>
        <div class="product__info">
            <div class="col-xs-4">
                <span class="product__title">Букет №{{ $product->id }}</span>
            </div>
            <div class="col-xs-8 text-right">
                @if($product->is_sale == 1)
                    <span class="product__price">
                        <span class="product__price-old">
                            {{ round($product->price) }} р.
                        </span>
                        {{ round($product->sale_price) }} р.
                    </span>
                    @else
                        <span class="featured-item__price">
                            {{ round($product->price) }} р.
                        </span>
                @endif
            </div>
        </div>
    </div>
</div>