@extends('layouts.front.app')

@section('og')
    <meta property="og:type" content="home"/>
    <meta property="og:title" content="{{ config('app.name') }}"/>
    <meta property="og:description" content="{{ config('app.name') }}"/>
@endsection

@section('content')
    @include('front.products.featured')

    <!-- START: SECTION 2-->
    <section class="s" id="s2">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2>Самые свежие букеты</h2>
                </div>
            </div>
            <!-- START: PRODUCTS -->
            @if(!empty($mostFresh))
                <div class="row products">
                    @foreach($mostFresh as $product)
                        @include('layouts.front.product', ['product' => $product])
                    @endforeach
                </div>
            @endif

            <div class="products__footer">
                <a href="{{ url('category/bukety') }}">Смотреть все букеты</a>
            </div>

        </div>
    </section>
    <!-- END: SECTION 2-->
    <!-- START: SECTION 3 -->
    <section class="s s-primary" id="s3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="text-center">Категории букетов</h2>
                </div>
            </div>
            <!-- START: CATEGORIES -->
            <div id="categories" class="row categories-cover swiper-container">
                <div class="categories-wrapper">

                    @foreach($categoriesBlade as $k => $category)
                        <div class="col-xs-12 {{ ($k != 0 && $k % 3 == 0) ? 'col-sm-8' : 'col-sm-4' }} category-cover swiper-slide">
                            <a href="{{ route('front.category.slug', $category->slug) }}">
						<span class="category-cover__image"
                              style="background-image: url({{ asset("storage/$category->cover") }})">
							<span class="category-cover__title">{{ $category->name }}</span>
							<span class="category-cover__overlay"></span>
							<span class="category-cover__icon">
								<i class="icon {{ $category->cover_icon }}"></i>
							</span>
						</span>
                            </a>
                        </div>
                    @endforeach


                </div>

            </div>
            <!-- END: CATEGORIES -->

            <!-- START: PAGINATION -->
            <div class="swiper-pagination visible-xs"></div>
        </div>
    </section>
    <!-- END: SECTION 3 -->
@endsection