@extends('layouts.front.app')

@section('content')
    <section class="cart">
        <!-- START: BREADCRUMBS -->
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumbs">
                        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a
                                    href="{{ route('home') }}" itemprop="url">Главная</a></li>
                        <li class="sep">&gt;</li>
                        <li class="current">Корзина</li>
                    </ul><!-- .breadcrumbs -->
                </div>
            </div>
        </div>
        <!-- END: BREADCRUMBS -->
        <div class="container">
            {{--<form class="order-form" action="{{ route('checkout.index') }}" method="post">--}}

            @if(!$cartItems->isEmpty())
                <div class="row">
                    @include('layouts.errors-and-messages')
                    <h1>Корзина</h1>
                    <div class="custom-menu">
                        <div class="menu-controls cart-menu">
                            <button class="menu-button menu-button-active">Готовые к заказу <span
                                        data-lines="1">{{ $countItems }}</span></button>
                            @if(isset($customer) && $customer->favorites->count() > 0)
                                <button class="menu-button">Отложенные <span>{{ $customer->favorites->count() }}</span>
                                </button>
                            @endif
                            <span data-cart-delete="-1"><a href="{{ route('cart.clear') }}">Очистить корзину</a></span>
                        </div>
                        <main class="menu-content col-xs-12 col-sm-6 order-menu-wrap">
                            <div class="cart-container">
                                <h2>Товары в корзине <span data-lines="1">{{ $countItems }}</span></h2>
                                @foreach($cartItems as $cartItem)
                                    <div class="cart-prod-wrapper">
                                        <div class="cart-prod-image-wrapper">
                                            @if($cartItem->product->is_sale == 1)
                                                <span class="badge">-{{ 100 - round($cartItem->product->sale_price*100/$cartItem->product->price) }}
                                                    %</span>
                                            @endif
                                            <a href="{{ route('front.get.product', [$cartItem->product->slug]) }}">
                                                <img class="cart-prod-image img img-responsive"
                                                     src="{{$cartItem->cover}}" alt="{{$cartItem->name}}">
                                            </a>
                                        </div>

                                        <div class="cart-prod-attr-wrapper">
                                            <div class="cart-prod-title">
                                                <a href="{{ route('front.get.product', [$cartItem->product->slug]) }}">{{$cartItem->name}}</a>
                                                <form action="{{ route('cart.destroy', $cartItem->rowId) }}"
                                                      method="post">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="delete">
                                                    <button data-cart-delete="{{ $cartItem->id }}"></button>
                                                </form>

                                            </div>

                                            <div class="cart-prod-size">
                                                <div class="cart-prod-size-label">Размер букета:</div>
                                                <div class="cart-prod-size-btns button-group">
                                                    @foreach($cartItem->options as $k => $option)
                                                        @if($k == 'product_attribute')
                                                            <button class="button-group-item button-group-item-active cart-option-but">{{ $option->name }}</button>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>

                                            <div class="cart-prod-qty-group">
                                                <div class="cart-prod-price-wrapper">
                                                    <div class="cart-prod-price text-center">{{ $cartItem->price }} р.
                                                    </div>
                                                    <div>Цена за шт.</div>
                                                </div>

                                                <div class="cart-prod-qty-wrapper">
                                                    <div class="cart-prod-qty text-center">
                                                        <span class="minus">-</span>
                                                        <input data-qty="{{ $cartItem->qty }}"
                                                               data-id="{{ $cartItem->rowId }}" type="text"
                                                               disabled
                                                               value="{{ $cartItem->qty }}">
                                                        <span class="plus">+</span>
                                                    </div>
                                                    <div>шт.</div>
                                                </div>

                                                <div class="cart-prod-price-wrapper">
                                                    <div class="cart-prod-price text-center cart-product-subtotal">
                                                        {{ $cartItem->price*$cartItem->qty }} р.
                                                    </div>
                                                    <div class="cart-total-text">Итого</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cart-envelope">
                                        <div class="custom-checkbox">
                                            <input id="{{ $cartItem->rowId }}" data-id="{{ $cartItem->rowId }}"
                                                   class="usePostCard"
                                                   onchange="Postcards.use('{{ $cartItem->rowId }}');"
                                                   type="checkbox" {{ ($cartItem->options['usePostCard'] == 'use') ? 'checked' : ' ' }}>
                                            <label class="postCardLavel" for="{{ $cartItem->rowId }}"></label>
                                            <span>Хочу добавить в букет открытку с пожеланиями</span>
                                        </div>
                                        <div class="cart-envelope-price">{{ $cartItem->options['postCardPrice']*$cartItem->qty }}
                                            р.
                                        </div>
                                    </div>
                                    <div class="cart-wishes">
                                        <div>Напишите свое пожелание*</div>
                                        <textarea
                                                oninput="Postcards.setWishes('{{ $cartItem->rowId }}', $(this).val());"
                                                maxlength="300"
                                                cols="10"
                                                rows="3">{{ trim($cartItem->options['postCardText'], ' ') }}</textarea>
                                    </div>
                                @endforeach

                                <div>Введите код купона для скидки</div>
                                <div class="flex-wrap-space сoup-wrap">
                                    <div>
                                        <div class="coupon-desc-block">
                                            <div class="coupon-success">
                                                @if(session()->has('coupon'))
                                                    <p>Купон применен: {{ session()->get('coupon.caption') }} <br>
                                                        (скидка не
                                                        действует
                                                        на акционные товары)</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="coupon-caption"></div>
                                        <div class="cart-coupon-group">
                                            <div class="rich-input">
                                                <input class="rich-input-field" type="text" id="coupon"
                                                       @if(session()->has('coupon')) value="{{ session()->get('coupon.code') }}" @endif>
                                                <div id="coupon-buttons">
                                                    @if(session()->has('coupon'))
                                                        <button class="btn btn-primary custom-button rich-input-addon"
                                                                id="couponDestroy" onclick="Coupons.couponDestroy();">
                                                            Удалить
                                                        </button>
                                                    @else
                                                        <button class="btn btn-primary custom-button rich-input-addon"
                                                                id="couponSubmit" onclick="Coupons.couponSubmit();">
                                                            Применить
                                                        </button>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="desc-sale-price">
                                        <div class="discount-block-left flex-wrap-space">
                                            @if(session()->has('coupon'))
                                                <div class="discount-item-desc">Скидка:</div>
                                                <div class="discount-item">{{ round($couponDiscount) }} р.</div>
                                            @endif
                                        </div>

                                        <div class="discount-block-right flex-wrap-space">
                                            <div class="cart-coupon-group-qty">Товаров: <span class="count-items"
                                                                                              data-lines="1">{{ $countItems }}</span>
                                            </div>
                                            <div class="cart-coupon-group-price">{{ round($subTotal) }} р.</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="goto-home">
                                    <a href="{{ route('home') }}">
                                        <button class="continue-shopping-btn" type="button">Продолжить покупки</button>
                                    </a>
                                </div>
                            </div>
                            @if(isset($customer))
                            <div class="cart-container">
                                <h2>Отложенные <span data-lines="1">{{ $customer->favorites->count() }}</span></h2>
                                    @foreach($customer->favorites as $product)
                                        <div class="cart-prod-wrapper cart-prod-pending account-favorite-item">
                                            <div class="cart-prod-image-wrapper">
                                                @if($product->is_sale == 1)
                                                    <span class="badge">-{{ 100 - round($cartItem->product->sale_price*100/$cartItem->product->price) }}
                                                    %</span>
                                                @endif
                                                <a href="{{ route('front.get.product', str_slug($product->slug)) }}">
                                                    <img class="cart-prod-image img img-responsive"
                                                         src="{{ asset("storage/$product->cover") }}" alt="{{ $product->name }}">
                                                </a>
                                            </div>

                                            <div class="cart-prod-attr-wrapper">
                                                <div class="cart-prod-title">
                                                    <a href="{{ route('front.get.product', str_slug($product->slug)) }}">{{ $product->name }}</a>
                                                    <span data-cart-delete="none" data-toggle="featured" data-id="{{ $product->id }}"></span>
                                                </div>

                                                <div class="cart-prod-size">
                                                    <div class="account-product-sku">Артикул: {{ $product->sku }}</div>
                                                    <div class="cart-prod-size-label">Размер букета:</div>
                                                    <div class="cart-prod-size-btns button-group">
                                                        @foreach($product->attributesValues as $attr)
                                                            @if($attr->attribute->name == 'Размеры')
                                                                <button class="button-group-item button-group-item-favorites"
                                                                        data-procent="{{ $attr->value }}" data-id="{{ $attr->id }}"
                                                                        data-from="feature">{{ $attr->name }}</button>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>

                                                <div class="cart-prod-qty-group cart-prod-qty-group-featured">
                                                    <span class="cart-featured {{ $product->isFeatured() }}" data-toggle="featured"
                                                          data-id="{{ $product->id }}"></span>
                                                    <div class="cart-prod-price "><span class="current-price">{{ round($product->price) }}</span> р.</div>
                                                    <div class="cart-prod-add-to-cart">
                                                        <form action="{{ route('cart.store') }}" class="form-inline d-flex align-class" method="post">
                                                            <a href="javascript:void(0);" onclick="this.closest('form').submit();">Добавить
                                                                в
                                                                корзину</a>
                                                            {{ csrf_field() }}
                                                            <input type="text" name="productAttribute" class="productAttribute" hidden>
                                                            <input type="hidden"
                                                                   class="form-control"
                                                                   name="quantity"
                                                                   id="quantity"
                                                                   placeholder="Quantity"
                                                                   value="1"/>
                                                            <input type="hidden" name="product" value="{{ $product->id }}"/>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                <div class="goto-home">
                                    <a href="{{ route('home') }}">
                                        <button class="continue-shopping-btn" type="button">Продолжить покупки</button>
                                    </a>
                                </div>
                            </div>
                            @endif
                        </main>
                    </div>
                    @include('front.carts.order-form')
                </div>
            @else
                @include('front.carts.cart-empty')
            @endif
            {{--</form>--}}
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('assets/js/jquery.mask.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('input[data-double="phone"]').mask('+7 (000) 000 00 00');
        })
    </script>
@endsection