@extends('layouts.front.app')

@section('content')
    <section class="cart">
        <!-- START: BREADCRUMBS -->
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumbs">
                        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a
                                    href="{{ route('home') }}" itemprop="url">Главная</a></li>
                        <li class="sep">&gt;</li>
                        <li class="current">Корзина</li>
                    </ul><!-- .breadcrumbs -->
                </div>
            </div>
        </div>
        <!-- END: BREADCRUMBS -->
        <div class="container">
                <div class="row">
                    @include('layouts.errors-and-messages')
                    <h1>Корзина</h1>
                    <div class="menu-controls cart-menu">
                        <a href="{{ route('cart.index') }}" class="menu-button ready-order-button">Готовые к заказу <span
                                    data-lines="1">{{ $countItems }}</span>
                        </a>
                        @if(isset($customer))
                            <button class="menu-button menu-button-active cart-favorites-button">Отложенные <span>{{ $customer->favorites->count() }}</span>
                            </button>@endif
                        <span></span>
                    </div>
                    <main class="menu-content col-xs-12 col-sm-12 order-menu-wrap">
                        <div class="cart-container">
                            @if(isset($customer))
                                @foreach($customer->favorites as $product)
                                    <div class="account-favorite-item row">
                                        <div class="col-xs-5 col-sm-3" style="padding: 0">
                                            <img src="{{ asset("storage/$product->cover") }}" alt="{{ $product->name }}">
                                        </div>
                                        <div class="col-xs-7 col-sm-9">
                                            <div class="account-favorite-item-right-column">
                                                <div class="account-product-title"><a
                                                            href="{{ route('front.get.product', str_slug($product->slug)) }}">{{ $product->name }}</a>
                                                </div>
                                                <div class="account-favorite-delete" data-cart-delete="account-favorite-delete" data-toggle="featured"
                                                     data-id="{{ $product->id }}" ></div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-9 padding-no">
                                            <div class="col-xs-12 col-sm-8" style="padding: 0">
                                                <div class="account-product-sku">Артикул: {{ $product->sku }}</div>
                                                <div class="account-product-size-wrap">
                                                    <div class="account-product-size">Размер букета:</div>
                                                    <div class="button-group">
                                                        @foreach($product->attributesValues as $attr)
                                                            @if($attr->attribute->name == 'Размеры')
                                                                <button class="button-group-item"
                                                                        data-procent="{{ $attr->value }}" data-id="{{ $attr->id }}"
                                                                        data-from="feature">{{ $attr->name }}</button>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 favorite-add-to-cart" style="padding: 0">
                                                <div class="d-flex align-class">
                                                    <div class="account-featured {{ $product->isFeatured() }}" data-toggle="featured"
                                                         data-id="{{ $product->id }}"></div>
                                                    <div class="account-favorite-item-price"><span
                                                                class="current-price">{{ round($product->price) }}</span> р.
                                                    </div>
                                                </div>

                                                <form action="{{ route('cart.store') }}" class="form-inline d-flex align-class" method="post">
                                                    <a href="javascript:void(0);" onclick="this.closest('form').submit();">Добавить
                                                        в
                                                        корзину</a>
                                                    {{ csrf_field() }}
                                                    <input type="text" name="productAttribute" class="productAttribute" hidden>
                                                    <input type="hidden"
                                                           class="form-control"
                                                           name="quantity"
                                                           id="quantity"
                                                           placeholder="Quantity"
                                                           value="1"/>
                                                    <input type="hidden" name="product" value="{{ $product->id }}"/>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            <div class="goto-home">
                                <a href="{{ route('home') }}">
                                    <button class="continue-shopping-btn" type="button">Продолжить покупки</button>
                                </a>
                            </div>
                        </div>
                    </main>
                </div>
        </div>


    </section>
@endsection