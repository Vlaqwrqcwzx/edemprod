<main class="empty-cart col-xs-12 col-sm-12">
    <h1>Корзина пуста</h1>
    <div class="post-message">
        🙈 Упс, кажется Вы еще ничего не положили в корзинку!<br>
        Воспользуйтесь поиском или просто полазайте по сайту в поисках красивых букетов!
        <div>
            <a href="{{ route('home') }}" class="btn btn-primary">Начать покупки</a>
        </div>
    </div>
</main>