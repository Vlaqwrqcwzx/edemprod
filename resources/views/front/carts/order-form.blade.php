<aside class="col-xs-12 col-sm-6">
    <form class="order-form" id="order-reg" action="{{ route('checkout.store') }}" method="post">
        @csrf
        <h2>Оформление заказа</h2>

        <div class="order-reg-delivery">
            <h3 class="ico-h-checked">Доставка</h3>

            <div class="button-group">
                @foreach($couriers as $courier)
                    <div class="order-reg-delivery-tile button-group-item {{ ($defaultCourier->id == $courier->id) ? 'button-group-item-active' : ' ' }}"
                         onclick="Couriers.setCourier('{{ $courier->id }}',{{ $courier->isFree() }})">
                        @if(isset($courier->image))
                            <img width="35%" src="{{ asset("storage/".$courier->image) }}" alt="{{ $courier->name }}">
                        @endif
                        <div>{{ $courier->name }}</div>
                        <div>{{ round($courier->cost) }} руб.</div>
                    </div>
                @endforeach
                <div class="order-reg-delivery-address">{{ session()->has('city') ? session()->get('city.address') : $defaulCity->address }}
                    <a href="{{ session()->has('city') ? session()->get('city.map_href') : $defaulCity->map_href }}" target="_blank">Посмотреть на карте</a>
                </div>
            </div>
        </div>


        <div class="order-reg-payment">
            <h3 class="ico-h-checked">Оплата</h3>
            <div class="button-group">
                <div class="order-reg-payment-tile button-group-item button-group-item-active"
                     id="bank-card">
                    {!! file_get_contents('icons/credit-card.svg') !!}
                    <div>Банковская карта</div>
                </div>
            </div>
        </div>


        <div class="order-reg-buyer">
            <h3>Покупатель</h3>

            <div class="order-reg-buyer-name">
                <div>Ваше имя*</div>
                <input class="form-control" type="text" required="" name="name"
                       value="{{ $customer->name ?? @old('name') }}">
            </div>

            <div class="order-reg-buyer-phone">
                <div>Телефон*</div>
                <input type="text" name="phone" data-double="phone" class="form-control"
                       data-mask="+0 (000) 000 00 00" placeholder="+7 (___) ___ __ __"
                       autocomplete="off" maxlength="18" required
                       value="{{ $customer->phone ?? @old('phone') }}"
                >
            </div>

            <div class="order-reg-buyer-email">
                <div>E-mail*</div>
                <input class="form-control" type="text" id="email" name="email" required=""
                       value="{{ $customer->email ?? @old('email') }}">
            </div>

            <div class="order-reg-buyer-delivery-time">
                <div>Время получения</div>
                <div class="d-flex align-items-center">
                    <select class="custom-select" name="time">
                        <option value="с 08:00 до 12:00"
                                @if(session()->get('cart.time') == 'с 08:00 до 12:00') selected @endif>с 08:00 до 12:00
                        </option>
                        <option value="с 12:00 до 18:00"
                                @if(session()->get('cart.time') == 'с 12:00 до 18:00') selected @endif>с 12:00 до 18:00
                        </option>
                        <option value="с 18:00 до 21:00"
                                @if(session()->get('cart.time') == 'с 18:00 до 21:00') selected @endif>с 18:00 до 21:00
                        </option>
                    </select>
                    <div class="mobile-tooltip"
                         data-tooltip="Уважаемый {{ $customer ? $customer->name : 'покупатель' }}, позвоните нам, как можно раньше, если ваши планы изменились, чтобы ваш букет был максимально свежим.">
                        <img src="{{ asset('icons/info_hover.svg') }}"
                             alt="Info">
                    </div>
                </div>
            </div>

            <div class="order-reg-buyer-street address-hide-wrap @if($defaultCourier->isFree()) hide @endif">
                <div>Улица*</div>
                <input name="billing_address" class="form-control" type="text"
                       value="{{ session()->get('cart.billing_address') ?? @old('billing_address')}}">
            </div>

            <div class="order-reg-buyer-house address-hide-wrap @if($defaultCourier->isFree()) hide @endif">
                <div>Дом*</div>
                <input name="house" class="form-control" type="text"
                       value="{{ session()->get('cart.house') ?? @old('house')}}">
            </div>

            <div class="order-reg-buyer-building address-hide-wrap @if($defaultCourier->isFree()) hide @endif">
                <div>Корпус</div>
                <input name="corpus" class="form-control" type="text"
                       value="{{ session()->get('cart.corpus') ?? @old('corpus')}}">
            </div>

            <div class="order-reg-buyer-apartment address-hide-wrap @if($defaultCourier->isFree()) hide @endif">
                <div>Квартира</div>
                <input name="flat" class="form-control" type="text"
                       value="{{ session()->get('cart.flat') ?? @old('flat')}}">
            </div>

            <div class="order-reg-buyer-intercom address-hide-wrap @if($defaultCourier->isFree()) hide @endif">
                <div>Домофон</div>
                <input name="domofon" class="form-control" type="text"
                       value="{{ session()->get('cart.domofon') ?? @old('domofon')}}">
            </div>

            <div class="order-reg-buyer-office address-hide-wrap @if($defaultCourier->isFree()) hide @endif">
                <div>Офис</div>
                <input name="office" class="form-control" type="text"
                       value="{{ session()->get('cart.office') ?? @old('office')}}">
            </div>


            <div class="order-reg-buyer-delivery-date">
                <div>Дата доставки заказа</div>
                <input name="date" class="form-control form-control-date" type="text"
                       value="{{ session()->get('cart.date') ?? @old('date') ?? \Carbon\Carbon::now()->format('d.m.yy')}}">
            </div>

            <div class="order-reg-buyer-comment">
                <div>Комментарий к заказу</div>
                <textarea name="comment" cols="10" rows="3"
                          placeholder="Укажите имя получателя, телефон или любую важную для нас информацию">{{ session()->get('cart.comment') ?? @old('comment')}}</textarea>
            </div>

        </div>


        <div class="order-reg-amount">

            <div>
                <div class="order-reg-amount-item">
                    <div>Товаров на сумму:</div>
                    <div class="subtotal-item">{{ round($subTotal) }} р.</div>
                </div>

                <div class="order-reg-amount-item">
                    <div>Доставка:</div>
                    <div class="delivery-item">{{ round($shippingFee) }} р.</div>
                </div>

                <div class="order-reg-amount-item">
                    <div>Открытка:</div>
                    <div class="postcard-item">{{ $postCard }} р.</div>
                </div>

                <div class="order-reg-amount-item discount-block">
                    @if(session()->has('coupon'))
                        <div>Скидка:</div>
                        <div class="discount-item">{{ round($couponDiscount) }} р.</div>
                    @endif
                </div>

                <div class="order-reg-amount-item">
                    <div>Итого к оплате:</div>
                    <div class="total-item">{{ round($total) }}
                        р.
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary custom-button">Оформить заказ</button>
        </div>
    </form>

    <div class="order-reg-agreement">
        <div>Нажимая на кнопку “Оформить заказ”, вы соглашаетесь на обработку</div>
        <a href="#">персональных данных интернет-магазина Эдемский Сад</a>
    </div>

</aside>