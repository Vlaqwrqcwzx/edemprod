<div role="tabpanel" class="tab-pane @if(request()->input('tab') == 'history')active @endif" id="history">
    <main class="col-xs-12 col-sm-9">
        <div class="account-order-wrapper">
            {{--<div class="account-sort">--}}
                {{--<select class="custom-select select2-hidden-accessible" tabindex="-1" aria-hidden="true"--}}
                        {{--data-select2-id="select2-data-5-b09s">--}}
                    {{--<option value="1" data-select2-id="select2-data-7-pqar">За последний месяц</option>--}}
                    {{--<option value="2">За последние три месяца</option>--}}
                {{--</select>--}}
            {{--</div>--}}
            @if(!$orders->isEmpty())
                @foreach($orders as $order)
                    @if($order['status']->name == 'ordered')
                        <div class="account-order">
                            <div class="account-order-header flex-row">
                                <div class="account-order-number">Заказ #{{ $order['id'] }}</div>
                                <div class="account-order-date">
                                    от {{ Carbon\Carbon::parse($order['created_at'])->format('d.m.yy H:i') }}</div>
                                <div class="account-order-status">Выполнен</div>
                                <a id="collapse-order{{$order['id']}}" class="collapse-order" data-toggle="collapse"
                                   href="#collapseOrder{{$order['id']}}"
                                   aria-expanded="true"
                                   aria-controls="collapseOrder" role="button">
                                    <img src="{{ asset('icons/arrow-collapse.svg') }}"
                                         alt="Expand/Collapse arrow">
                                </a>
                            </div>

                            <div class="collapse" id="collapseOrder{{$order['id']}}">
                                <div class="account-order-info-section">
                                    <div class="account-order-info-title">Информация о заказе</div>
                                    <div class="account-order-info-row flex-row">
                                        {{--<div>Товаров в заказе: </div>--}}
                                    </div>
                                    <div class="account-order-info-row flex-row">
                                        <div>Дата и время доставки:</div>
                                        <div>
                                            <span>{{ Carbon\Carbon::parse($order['shipping_date'])->format('d.m.Y')}} {{ $order['shipping_time'] }}</span>
                                        </div>
                                    </div>
                                    <div class="account-order-info-row flex-row">
                                        <div>Товаров на сумму:</div>
                                        <div>
                                            <span>{{round($order['total_products'])}} {{ config('cart.currency_symbol') }}</span>
                                        </div>
                                    </div>
                                    <div class="account-order-info-row flex-row">
                                        <div>Доставка:</div>
                                        <div>
                                            <span>{{ $order['courier']['name'] }}, {{round($order['total_shipping'])}} {{ config('cart.currency_symbol') }}</span>
                                        </div>
                                    </div>
                                    <div class="account-order-info-row flex-row">
                                        <div>Открытки:</div>
                                        <div>
                                            <span>{{round($order['total_postcards'])}} {{ config('cart.currency_symbol') }}</span>
                                        </div>
                                    </div>
                                    <div class="account-order-info-row flex-row">
                                        <div>Скидка:</div>
                                        <div>
                                            <span>{{round($order['discounts'])}} {{ config('cart.currency_symbol') }}</span>
                                        </div>
                                    </div>
                                    <div class="account-order-info-row flex-row">
                                        <div>К оплате:</div>
                                        <div>
                                            <span>{{round($order['total'])}} {{ config('cart.currency_symbol') }}</span>
                                        </div>
                                    </div>
                                    <div class="account-order-info-row flex-row">
                                        <div>Доставка по адресу:</div>
                                        <div>{{ $order['city']['name'] }}, {{ $order['address']['billing_address'] }}
                                            , {{ $order['address']['house'] }}, {{ $order['address']['corpus'] }}
                                            , {{ $order['address']['flat'] }}, {{ $order['address']['domofon'] }}
                                            , {{ $order['address']['office'] }}</div>
                                    </div>
                                </div>

                                <div class="account-order-content-section">
                                    <div class="account-order-content-title">Содержимое заказа</div>
                                    @foreach ($order['products'] as $product)
                                        <div class="account-product flex-row">
                                            <img src="{{ asset("storage/".$product['cover']) }}"
                                                 alt="{{ $product['name'] }}">
                                            <div class="account-product-common-column">
                                                <div class="account-product-title">{{ $product['name'] }}</div>
                                                <div class="account-product-sku">Артикул: {{ $product['sku'] }}</div>
                                                <div class="account-product-size">Размер букета:</div>
                                                <button class="button-group-item">{{ \App\Shop\AttributeValues\AttributeValue::find($product['pivot']['product_attribute_id'])->name }}</button>
                                                <div>
                                                    @if($product['pivot']['use_postcard'] == 'use')
                                                        <p>В букет добавлена открытка с пожеланиями</p>
                                                        <p>Текст: {!! $product['pivot']['postcard_text'] ?? '' !!}</p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="account-product-price-column text-center">
                                                <div>{{ round($product['pivot']['product_price']) }} {{ config('cart.currency_symbol') }}</div>
                                                <div>Цена за шт.</div>
                                            </div>
                                            <div class="account-product-qty-column text-center">
                                                <div>{{$product['pivot']['quantity']}}</div>
                                                <div>шт.</div>
                                            </div>
                                            <div class="account-product-amount-column text-center">
                                                <div>{{ round($product['pivot']['product_price']*$product['pivot']['quantity']) }} {{ config('cart.currency_symbol') }}</div>
                                                <div>Итого</div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @else
                <main class="empty-cart col-xs-12 col-sm-12">
                    <h1>История заказов</h1>
                    <div class="post-message">
                        🙈 Упс, кажется Вы еще ничего не положили в корзинку!<br>
                        Воспользуйтесь поиском или просто полазайте по сайту в поисках красивых букетов!
                        <div>
                            <a href="{{ route('home') }}" class="btn btn-primary">Начать покупки</a>
                        </div>
                    </div>
                </main>
            @endif
        </div>
    </main>
</div>