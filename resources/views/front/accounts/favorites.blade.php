<div role="tabpanel" class="tab-pane @if(request()->input('tab') == 'favorites')active @endif" id="favorites">
        <main class="col-xs-12 col-sm-9">
            <div class="account-favorites-wrapper">
                @if(!$favorites->isEmpty())
                <div class="account-favorites-header flex-row">
                    <span data-cart-delete="none"><a href="{{ route('clear.favorites') }}" class="delFavorites">Очистить избранное</a></span>
                    {{--                <select class="custom-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">--}}
                    {{--                    <option value="1">По возрастанию цены</option>--}}
                    {{--                    <option value="2">По убыванию цены</option>--}}
                    {{--                </select>--}}
                </div>
                <div class="account-favorites-block">
                    @foreach($favorites as $product)
{{--                        <div class="account-favorite-item row">--}}
{{--                            <div class="col-xs-5 col-sm-3" style="padding: 0">--}}
{{--                                <img src="{{ asset("storage/$product->cover") }}" alt="{{ $product->name }}">--}}
{{--                            </div>--}}
{{--                            <div class="col-xs-7 col-sm-9">--}}
{{--                                <div class="account-favorite-item-right-column">--}}
{{--                                    <div class="account-product-title"><a--}}
{{--                                                href="{{ route('front.get.product', str_slug($product->slug)) }}">{{ $product->name }}</a>--}}
{{--                                    </div>--}}
{{--                                    <div class="account-favorite-delete" data-cart-delete="account-favorite-delete" data-toggle="featured"--}}
{{--                                         data-id="{{ $product->id }}"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="col-xs-12 col-sm-9 padding-no">--}}
{{--                                <div class="col-xs-12 col-sm-8" style="padding: 0">--}}
{{--                                    <div class="account-product-sku">Артикул: {{ $product->sku }}</div>--}}
{{--                                    <div class="account-product-size-wrap">--}}
{{--                                        <div class="account-product-size">Размер букета:</div>--}}
{{--                                        <div class="button-group">--}}
{{--                                            @foreach($product->attributesValues as $attr)--}}
{{--                                                @if($attr->attribute->name == 'Размеры')--}}
{{--                                                    <button class="button-group-item"--}}
{{--                                                            data-procent="{{ $attr->value }}" data-id="{{ $attr->id }}"--}}
{{--                                                            data-from="feature">{{ $attr->name }}</button>--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-xs-12 col-sm-4 favorite-add-to-cart" style="padding: 0">--}}
{{--                                    <div class="d-flex align-class">--}}
{{--                                        <div class="account-featured {{ $product->isFeatured() }}" data-toggle="featured"--}}
{{--                                             data-id="{{ $product->id }}"></div>--}}
{{--                                        <div class="account-favorite-item-price"><span--}}
{{--                                                    class="current-price">{{ round($product->price) }}</span> р.--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <form action="{{ route('cart.store') }}" class="form-inline d-flex align-class" method="post">--}}
{{--                                        <a href="javascript:void(0);" onclick="this.closest('form').submit();">Добавить--}}
{{--                                                в--}}
{{--                                                корзину</a>--}}
{{--                                        {{ csrf_field() }}--}}
{{--                                        <input type="text" name="productAttribute" class="productAttribute" hidden>--}}
{{--                                        <input type="hidden"--}}
{{--                                               class="form-control"--}}
{{--                                               name="quantity"--}}
{{--                                               id="quantity"--}}
{{--                                               placeholder="Quantity"--}}
{{--                                               value="1"/>--}}
{{--                                        <input type="hidden" name="product" value="{{ $product->id }}"/>--}}
{{--                                    </form>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="cart-prod-wrapper cart-prod-pending account-favorite-item p-3">
                            <div class="cart-prod-image-wrapper">
                                @if($product->is_sale == 1)
                                    <span class="badge">-{{ 100 - round($product->sale_price*100/$product->price) }}
                                                    %</span>
                                @endif
                                <a href="{{ route('front.get.product', str_slug($product->slug)) }}">
                                    <img class="cart-prod-image img img-responsive"
                                         src="{{ asset("storage/$product->cover") }}" alt="{{ $product->name }}">
                                </a>
                            </div>

                            <div class="cart-prod-attr-wrapper">
                                <div class="cart-prod-title">
                                    <a href="{{ route('front.get.product', str_slug($product->slug)) }}">{{ $product->name }}</a>
                                    <span data-cart-delete="none" data-toggle="featured" data-id="{{ $product->id }}"></span>
                                </div>

                                <div class="cart-prod-size">
                                    <div class="account-product-sku">Артикул: {{ $product->sku }}</div>
                                    <div class="cart-prod-size-label">Размер букета:</div>
                                    <div class="cart-prod-size-btns button-group">
                                        @foreach($product->attributesValues as $attr)
                                            @if($attr->attribute->name == 'Размеры')
                                                <button class="button-group-item button-group-item-favorites"
                                                        data-procent="{{ $attr->value }}" data-id="{{ $attr->id }}"
                                                        data-from="feature">{{ $attr->name }}</button>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>

                                <div class="cart-prod-qty-group cart-prod-qty-group-featured">
                                                    <span class="cart-featured {{ $product->isFeatured() }}" data-toggle="featured"
                                                          data-id="{{ $product->id }}"></span>
                                    <div class="cart-prod-price "><span class="current-price">{{ round($product->price) }}</span> р.</div>
                                    <div class="cart-prod-add-to-cart">
                                        <form action="{{ route('cart.store') }}" class="form-inline d-flex align-class" method="post">
                                            <a href="javascript:void(0);" onclick="this.closest('form').submit();">Добавить
                                                в
                                                корзину</a>
                                            {{ csrf_field() }}
                                            <input type="text" name="productAttribute" class="productAttribute" hidden>
                                            <input type="hidden"
                                                   class="form-control"
                                                   name="quantity"
                                                   id="quantity"
                                                   placeholder="Quantity"
                                                   value="1"/>
                                            <input type="hidden" name="product" value="{{ $product->id }}"/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>
                @else
                    <main class="empty-cart col-xs-12 col-sm-12">
                        <h1>Избранное</h1>
                        <div class="post-message">
                            🙈 Упс, кажется Вы еще ничего не добавили!<br>
                            Воспользуйтесь поиском или просто полазайте по сайту в поисках красивых букетов!
                            <div>
                                <a href="{{ route('home') }}" class="btn btn-primary">Начать покупки</a>
                            </div>
                        </div>
                    </main>
                @endif
            </div>
        </main>

</div>