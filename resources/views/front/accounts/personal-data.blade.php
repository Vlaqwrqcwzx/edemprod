<div role="tabpanel" class="tab-pane @if(request()->input('tab') == 'personal')active @endif" id="personal">
    <main class="col-xs-12 col-sm-9">
        <div class="account-personal-form" data-select2-id="select2-data-22-huy0">
            <h3>Контактные данные</h3>

            <form class="account-contact-form" method="POST" action="{{ route('update.personal.data', $customer) }}">
                @csrf
                <div class="account-row flex-row">
                    <div class="account-field">
                        <label for="surname">Фамилия</label>
                        <input type="text" class="form-control" name="surname" value="{{ $customer->surname }}">
                    </div>

                    <div class="account-field">
                        <label for="email">E-mail*</label>
                        <input type="text" class="form-control" name="email" value="{{ $customer->email }}" required>
                    </div>
                </div>

                <div class="account-row flex-row">
                    <div class="account-field">
                        <label for="username">Имя*</label>
                        <input type="text" class="form-control" name="name" value="{{ $customer->name }}" required>
                    </div>

                    <div class="account-field">
                        <label for="phone">Телефон*</label>
                        <input type="text" name="phone" data-double="phone" class="form-control"
                               data-mask="+0 (000) 000 00 00" placeholder="+7 (___) ___ __ __"
                               autocomplete="off" maxlength="18" required
                               value="{{ $customer->phone }}">
                    </div>
                </div>

                <div class="account-row flex-row">
                    <div class="account-field">
                        <label for="patronymic">Отчество</label>
                        <input type="text" class="form-control" name="patronymic" value="{{ $customer->patronymic }}">
                    </div>

                    <div class="account-field">
                        <label>Дата рождения*</label>

                        <div class="flex-row">
                            <input class="form-control" type="date" name="birthday" value="{{ Carbon\Carbon::parse($customer->birthday)->format('Y-m-d') }}">
                        </div>
                    </div>
                </div>

                <div class="account-row flex-row">
                    <div class="account-field">
                        <label>День рождения супруги(а)</label>
                        <div class="flex-row">
                            <input class="form-control" type="date" name="wife_birthday" value="{{ Carbon\Carbon::parse($customer->wife_birthday)->format('Y-m-d') }}">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary custom-button">Сохранить изменения</button>
            </form>
            <h3>Смена пароля</h3>

            <form class="account-change-password-form" method="POST" action="{{ route('change.password') }}">
                @csrf
                <div class="account-row flex-row">
                    <div class="account-field">
                        <label for="password">Пароль*</label>
                        <div class="rich-input rich-input-password">
                            <input class="rich-input-field" type="password" name="password" required>
                            <img class="rich-input-addon"
                                 src="{{ asset('icons/see.svg') }}" alt="See">
                        </div>
                    </div>

                    <div class="account-field">
                        <label for="new-password">Придумайте новый пароль*</label>
                        <div class="rich-input rich-input-password">
                            <input class="rich-input-field" type="password" name="new_password">
                            <img class="rich-input-addon"
                                 src="{{ asset('icons/see.svg') }}" alt="See">
                        </div>
                    </div>
                </div>

                <div class="account-repeat-password">
                    <label for="repeat-password">Повторите пароль*</label>
                    <div class="rich-input rich-input-password">
                        <input class="rich-input-field" type="password" name="new_confirm_password">
                        <img class="rich-input-addon"
                             src="{{ asset('icons/see.svg') }}" alt="See">
                    </div>
                </div>

                <button type="submit" class="btn btn-primary custom-button">Сменить пароль</button>
            </form>
        </div>
    </main>
</div>