@extends('layouts.front.app')

@section('og')
    <meta property="og:type" content="category"/>
    <meta property="og:title" content="{{ $category->name }}"/>
    <meta property="og:description" content="{{ $category->description }}"/>
    @if(!is_null($category->cover))
        <meta property="og:image" content="{{ asset("storage/$category->cover") }}"/>
    @endif
@endsection

@section('content')
    <section class="shop">
        <!-- START: BREADCRUMBS -->
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumbs">
                        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a
                                    href="{{ route('home') }}" itemprop="url">Главная</a></li>
                        <li class="sep">&gt;</li>
                        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{ route('front.category.slug', $category->slug) }}"
                               itemprop="url">Каталог</a>
                        </li>
                        <li class="sep">&gt;</li>
                        <li class="current">{{ $category->name }}</li>
                    </ul><!-- .breadcrumbs -->    </div>
            </div>
        </div>
        <!-- END: BREADCRUMBS -->
        <div class="container">
            <div class="row">
                <aside class="col-xs-12 col-sm-2">
                    <!-- START: SIDEBAR NAV -->
                    <nav class="sidebar-nav">
                        <ul class="list-unstyled">
                            @foreach($categories as $category)
                                <li @if(request()->segment(2) == $category->slug) class="active" @endif>
                                    <a href="{{ route('front.category.slug', $category->slug) }}">
                                        <i class="{{ $category->cover_icon }}"></i>{{ $category->name }}
                                    </a>
                                    @if($category->children->count() > 0)
                                        <ul class="sidebar-nav__sub-nav">
                                            @foreach($category->children as $sub)
                                                <li @if(request()->segment(2) == $sub->slug) class="active" @endif>
                                                    <a href="{{ route('front.category.slug', $sub->slug) }}">{{ $sub->name }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                    <!-- END: SIDEBAR NAV -->

                </aside>
                @if($products->count() > 0)
                    <main class="col-xs-12 col-sm-10">
                        {{--<div class="search-wrapper">--}}
                            {{--<form action="{{route('search.product')}}" method="GET" class="search-item">--}}
                                {{--<input type="text" name="q" placeholder="Поиск..." autocomplete="off" required>--}}
                                {{--<div class="search"></div>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                        <!-- /.search form -->
                        <div class="row" id="rc-products">
                            @foreach($products as $product)
                                @include('layouts.front.product', ['product' => $product])
                            @endforeach
                        </div>
                        <div class="row text-center">
                            {{ $products->links() }}
                        </div>
                    </main>
                @else
                    <main class="not-found col-xs-12 col-sm-10">

                        <h1>Ничего не нашли...</h1>

                        <div class="post-message">
                            <div>🙈 Упс, кажется мы не нашли ни букетов, ни товаров по запрашиваемым хэштегам.</div>
                            <div>
                                <a href="{{ route('home') }}" class="btn btn-primary">На главную</a>
                            </div>
                        </div>

                    </main>
                @endif
            </div>

        </div>
    </section>
@endsection
