@extends('layouts.front.app')

@section('content')
    <section>
        <!-- START: BREADCRUMBS -->
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumbs">
                        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a
                                    href="{{ route('home') }}" itemprop="url">Главная</a></li>
                        <li class="sep">&gt;</li>
                        <li class="current">{{ $page->title }}</li>
                    </ul><!-- .breadcrumbs -->
                </div>
            </div>
        </div>
        <!-- END: BREADCRUMBS -->
        <div class="container">
            <div class="row">
                <main class="col-xs-12 col-sm-9">
                    <div class="delivery-pay-wrapper">
                        <h1>{{ $page->title }}</h1>

                        {!! $page->text !!}
                    </div>
                </main>
            </div>
        </div>
    </section>
@endsection