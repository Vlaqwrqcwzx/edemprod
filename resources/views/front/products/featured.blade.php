<!-- START SECTION 1 -->
<section class="s s-primary" id="s1">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2>Авторские букеты в Москве</h2>
            </div>
        </div>

        <div class="row featured">

            @foreach($brandsWithLastProduct as $brand)
                <?php $product = $brand->lastProduct ?>
            @if(isset($product))
                <div class="col-xs-12 col-sm-4 featured-item">
                    <h3 class="ico-h {{ $brand->cover_icon }} text-center">{{ $brand->name }}</h3>
                    <div class="featured-item__body">
                        <!-- START: IMAGE -->
                        <div class="featured-item__images">
                            @if($product->is_sale == 1)
                                <span class="badge" title="Акция"> {{ 100 - round($product->sale_price*100/$product->price) }}
                                    %</span>
                            @endif
                            <span data-toggle="featured" data-id="{{ $product->id }}" class="{{ $product->isFeatured() }}"></span>
                            <a href="{{ route('front.get.product', str_slug($product->slug)) }}">
                                <img src="{{ asset("storage/".$product->cover) }}" alt="{{ $product->name }}"
                                     class="img-responsive featured-item__top-image">
                                <img src="{{ asset("storage/".$product->cover_hover) }}" alt="{{ $product->name }}"
                                     class="img-responsive featured-item__bot-image">
                            </a>
                            <span class="featured-item__tags">
								@foreach($product->attributesValues as $attr)
                                    @if($attr->attribute->name == 'Hashtags')
                                        <a href="javascript:void(0);" data-type="tag">{{ $attr->name }}</a>
                                    @endif
                                @endforeach
							</span>

                        </div>
                        <!-- END: IMAGE -->

                        <div class="featured-item__info">
                            <div class="col-xs-5">
                                <span class="featured-item__title">Букет №{{ $product->id }}</span>
                            </div>
                            <div class="col-xs-7 text-right">

                                @if($product->is_sale == 1)
                                    <span class="featured-item__price">
                                        <span class="featured-item__price-old">
                                        {{ round($product->price) }} р.
                                        </span>
                                        {{ round($product->sale_price) }} р.
                                    </span>
                                @else
                                    <span class="featured-item__price">{{ round($product->price) }} р.</span>
                                @endif

                            </div>
                        </div>

                    </div>
                    <div class="featured-item__footer">
                        <a href="{{ route('front.brand.slug', str_slug($brand->name)) }}">
                        @switch($brand->name)
                        @case('К празднику')
                            Ещё букеты к празднику
                        @break

                        @case('Акция')
                            Ещё букеты по акции
                        @break

                        @case('Популярное')
                            Все популярные букеты
                        @break

                        @default
                            Смотреть все букеты
                        @endswitch
                        </a>
                    </div>
                </div>
            @endif
            @endforeach

        </div>
    </div>
</section>