<div class="mobile-product-view">
    <div class="col-xs-12">
        <h1 class="single-product-title">
            {{ $product->name }} </h1>
        <div class="single-product-sku">
            Артикул: {{ $product->sku }}
        </div>
    </div>
    @if(isset($images) && !$images->isEmpty())
        <div id="productSlider" class="row swiper-container">
            <div class="categories-wrapper">
                <div class="col-xs-12 category-cover swiper-slide">
                    @if($product->is_sale == 1)
                        <span class="badge" title="Акция"> {{ 100 - round($product->sale_price*100/$product->price) }}
                                            %</span>
                    @endif
                    <a data-fancybox="gallery" href="{{ asset("storage/$product->cover") }}">
                                    <span class="category-cover__image"
                                          style="background-image: url({{ asset("storage/$product->cover") }})">
                                    </span>
                    </a>
                </div>
                <div class="col-xs-12 category-cover swiper-slide">
                    @if($product->is_sale == 1)
                        <span class="badge" title="Акция"> {{ 100 - round($product->sale_price*100/$product->price) }}
                                            %</span>
                    @endif
                    <a data-fancybox="gallery" href="{{ asset("storage/$product->cover_hover") }}">
                                    <span class="category-cover__image"
                                          style="background-image: url({{ asset("storage/$product->cover_hover") }})">
                                    </span>
                    </a>
                </div>
                @foreach($images as $image)
                    <div class="col-xs-12 category-cover swiper-slide">
                        @if($product->is_sale == 1)
                            <span class="badge" title="Акция"> {{ 100 - round($product->sale_price*100/$product->price) }}
                                            %</span>
                        @endif
                        <a data-fancybox="gallery" href="{{ asset("storage/$image->src") }}">
                                    <span class="category-cover__image"
                                          style="background-image: url({{ asset("storage/$image->src") }})">
                                    </span>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>

        <!-- START: PAGINATION -->
        <div class="swiper-pagination-custom visible-xs swiper-margins"></div>
    @endif
</div>