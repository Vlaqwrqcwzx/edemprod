@extends('layouts.front.app')

@section('og')
    <meta property="og:type" content="product"/>
    <meta property="og:title" content="{{ $product->name }}"/>
    <meta property="og:description" content="{{ strip_tags($product->description) }}"/>
    @if(!is_null($product->cover))
        <meta property="og:image" content="{{ asset("storage/$product->cover") }}"/>
    @endif
@endsection

@section('content')
    <section class="shop">
        <!-- START: BREADCRUMBS -->
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumbs">
                        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{ route('home') }}" itemprop="url">Главная</a></li>
                        <li class="sep">&gt;</li>
                        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{ route('front.get.product', str_slug($product->slug)) }}"
                               itemprop="url">Товар</a>
                        </li>
                        <li class="sep">&gt;</li>
                        <li class="current">{{ $product->name }}</li>
                    </ul><!-- .breadcrumbs -->
                </div>
            </div>
        </div>
        <!-- END: BREADCRUMBS -->
        <div class="container">
            <div class="row">
                @include('layouts.errors-and-messages')
            </div>

            @include('front.products.product-mobile')

            <div class="row">
                <main class="col-sm-12 col-xs-12">
                    <main class="single-product row">
                        <!-- START: SINGLE PRODUCTS IMAGES -->
                        <div class="img-slider__container desktop-product-view">
                            <div class="single-product-img-slider col-sm-1 col-xs-4">
                                <div class="img-slider">
                                    <div class="slider__wrapper">
                                        <img class="slider__item"
                                             src="{{ asset("storage/$product->cover") }}"
                                             alt="{{ $product->name }}">
                                        <img class="slider__item"
                                             src="{{ asset("storage/$product->cover_hover") }}"
                                             alt="{{ $product->name }}">
                                        @if(isset($images) && !$images->isEmpty())
                                            @foreach($images as $image)
                                                <img class="slider__item"
                                                     src="{{ asset("storage/$image->src") }}"
                                                     alt="{{ $product->name }}">
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="slider__controls">
                                        <a class="slider__control slider__control_top slider__control_top-inactive"
                                           href="#" role="button"></a>
                                        <a class="slider__control slider__control_bottom slider__control_bottom-active"
                                           href="#" role="button"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="single-product-selected-img col-sm-5 col-xs-8">
                                <div class="single-product-selected-img__main">
                                    @if($product->is_sale == 1)
                                        <span class="badge" title="Акция"> {{ 100 - round($product->sale_price*100/$product->price) }}
                                            %</span>
                                    @endif
                                    <a data-fancybox="gallery2"
                                       href="{{ asset("storage/$product->cover") }}"
                                       class="slider__selected-lightbox">
                                        <img src="{{ asset("storage/$product->cover") }}" alt="Product"
                                             class="slider__selected-img img img-responsive">
                                        <img class="zoom"
                                             src="{{ asset('icons/zoom.svg') }}"
                                             alt="Zoom">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- END: SINGLE PRODUCTS IMAGES -->

                        <div class="col-sm-6 col-xs-12">
                            <!-- START: SINGLE PRODUCTS TITLE -->
                            <h1 class="single-product-title desktop-product-view">
                                {{ $product->name }} </h1>
                            <div class="single-product-sku desktop-product-view">
                                Артикул: {{ $product->sku }}
                            </div>
                            <!-- END: SINGLE PRODUCTS TITLE -->
                            <!-- START: SINGLE PRODUCTS PRICES -->
                            <div class="single-product-featured {{ $product->isFeatured() }}" data-toggle="featured"
                                 data-id="{{ $product->id }}"></div>
                            <div class="single-product-prices">

                                @if($product->is_sale == 1)
                                    <span class="single-product-price">
                                        <span id="sale_price">{{ round($product->sale_price) }}</span> р.
                                    </span>

                                    <span class="single-product-price__old">
                                        <span id="price">{{ round($product->price) }}</span> р.
                                    </span>
                                @else
                                    <span class="single-product-price">
                                        <span id="price">{{ round($product->price) }}</span> р.
                                    </span>
                                @endif

                            </div>
                            <!-- END: SINGLE PRODUCTS PRICES -->

                            <!-- START: SINGLE PRODUCT SIZE -->
                            <div class="single-product-size">
                                <div class="single-product-size">Выберите размер букета:</div>
                                <div class="button-group">
                                    @foreach($product->attributesValues as $attr)
                                        @if($attr->attribute->name == 'Размеры')
                                            <button class="button-group-item"
                                                    data-procent="{{ $attr->value }}"
                                                    data-id="{{ $attr->id }}">{{ $attr->name }}</button>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <!-- END: SINGLE PRODUCT SIZE -->

                            <form action="{{ route('cart.store') }}" class="form-inline" method="post">
                                {{ csrf_field() }}
                                <input type="text" name="productAttribute" id="productAttribute" hidden>
                                <input type="hidden"
                                       class="form-control"
                                       name="quantity"
                                       id="quantity"
                                       placeholder="Quantity"
                                       value="1"/>
                                <input type="hidden" name="product" value="{{ $product->id }}"/>
                                <!-- START: SINGLE PRODUCTS ADD TO CART -->
                                <button type="submit" class="btn btn-primary btn-add-to-cart">
                                    <img src="{{ asset('icons/cart.svg') }} " alt="Cart">
                                    В корзину
                                </button>
                            </form>

                            <!-- START: SINGLE PRODUCT COMPOSITION -->
                            <div class="single-product-composition">
                                <div>Состав букета:</div>
                                <div class="single-product-composition-content">
                                    <div>
                                        {!! $product->description !!}
                                    </div>
                                    <div data-tooltip="Каждый букет, который мы создаем, уникален по-своему. Мы работаем с редкими и сезонными цветами и оставляем за собой авторское право вносить изменения состав букета, не нарушая его концепции, размера и цветового решения.">
                                        <img src="{{ asset('icons/info_hover.svg') }}"
                                             alt="Info">
                                    </div>
                                </div>
                            </div>

                            <div class="single-product-tags">
                                <?php $k = 0 ?>
                                @foreach($product->attributesValues as $attr)
                                    @if($attr->attribute->name == 'Hashtags')
                                        {{ ($k == 0) ? ' ' : ',' }}<a href="javascript:void(0);">#{{ $attr->name }}</a>
                                        <?php $k++ ?>
                                    @endif
                                @endforeach
                            </div>
                        </div>

                        <div class="single-product-more col-sm-12 col-xs-12">
                            <!-- START: SINGLE PRODUCT ADVANTAGES -->
                            <div class="single-product-advantages">
                                <div class="single-product-advantage advantage-delivery">
                                    <img src="{{ asset('icons/track.svg') }}"
                                         class="single-product-advantage-icon" alt="Track">
                                    <div>Экспресс-доставка букета по Москве от 60 минут с момента готовности заказа.
                                    </div>
                                </div>
                                <div class="single-product-advantage advantage-photo">
                                    <img src="{{ asset('icons/camera.svg') }}"
                                         class="single-product-advantage-icon" alt="Photo">
                                    <div>Фото букета перед отправкой. Только приятные сюрпризы!</div>
                                </div>
                                <div class="single-product-advantage advantage-sale">
                                    <img src="{{ asset('icons/sale.svg') }}"
                                         class="single-product-advantage-icon" alt="Sale">
                                    <div>Накопительные и персональные скидки!</div>
                                </div>
                            </div>
                            <!-- END: SINGLE PRODUCT ADVANTAGES -->

                            <!-- START: SINGLE PRODUCT SUB-MENU -->
                            <div class="custom-menu">
                                <div class="menu-controls">
                                    <button class="menu-button menu-button-active">Доставка</button>
                                    <button class="menu-button">Оплата</button>
                                    <button class="menu-button">Скидки</button>
                                </div>
                                <div class="menu-content">
                                    <div>
                                        @foreach($couriers as $courier)
                                            <div>{{ $courier->name }} -
                                                @if($courier->is_free == 0)
                                                    <span>{{ round($courier->cost) . ' руб.'}}</span>
                                                @else
                                                    <span>Бесплатно</span> {{ $courier->description ? '('. $courier->description . ')' : '' }}
                                                    <a href="{{ session()->has('city') ? session()->get('city.map_href') : $defaulCity->map_href }}" target="_blank">Посмотреть на карте</a>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                    <div>payment-content</div>
                                    <div>discount-content</div>
                                </div>
                            </div>
                            <!-- END: SINGLE PRODUCT SUB-MENU -->

                            @include('layouts.front.recomended')
                        </div>

                    </main>
                </main>
            </div>

        </div>
    </section>
@endsection