@extends('layouts.front.app')

@section('content')
    <section class="shop">
        <!-- START: BREADCRUMBS -->
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumbs">
                        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                            <a href="{{ route('home') }}" itemprop="url">Главная</a></li>
                        <li class="sep">&gt;</li>
                        <li class="current">Личный кабинет</li>
                    </ul><!-- .breadcrumbs -->
                </div>
            </div>
        </div>
        <!-- END: BREADCRUMBS -->
        <div class="container">
            <div class="row">
                @include('layouts.errors-and-messages')
                <div class="account-wrapper">
                    <aside class="col-xs-12 col-sm-3">
                        <div class="account-sidebar-wrapper">
                            <h1>Личный кабинет</h1>
                            <ul class="account-sidebar-menu" role="tablist">
                                <li role="presentation"
                                    class="menu-button admin-nav @if(request()->input('tab') == 'current') button-group-item-active @endif">
                                    <a href="{{ route('accounts', ['tab' => 'current']) }}">
                                        <img src="{{ asset('icons/clock.svg') }}"
                                             alt="Current orders">
                                        <span>Текущие заказы</span>
                                        {{--<div class="menu-counter">1</div>--}}
                                    </a>
                                </li>
                                <li role="presentation"
                                    class="menu-button admin-nav @if(request()->input('tab') == 'history') button-group-item-active @endif">
                                    <a href="{{ route('accounts', ['tab' => 'history']) }}">
                                        <img src="{{ asset('icons/history.svg') }}"
                                             alt="Order history">
                                        <span>История заказов</span>
                                        {{--<div class="menu-counter">1</div>--}}
                                    </a>
                                </li>
                                <li role="presentation"
                                    class="menu-button admin-nav @if(request()->input('tab') == 'personal') button-group-item-active @endif">
                                    <a href="{{ route('accounts', ['tab' => 'personal']) }}">
                                        <img src="{{ asset('icons/cabinet.svg') }}"
                                             alt="Personal data">
                                        <span>Личные данные</span>
                                    </a>
                                </li>
                                <li role="presentation "
                                    class="menu-button admin-nav @if(request()->input('tab') == 'favorites') button-group-item-active @endif">
                                    <a href="{{ route('accounts', ['tab' => 'favorites']) }}">
                                        <img src="{{ asset('icons/izbran-o.svg') }}"
                                             alt="Favorites">
                                        <span>Избранное</span>
                                        {{--<div class="menu-counter">{{ $favorites->count() }}</div>--}}
                                    </a>
                                </li>
                                <li class="menu-button admin-nav">
                                    <a href="{{ route('logout') }}">
                                        <img width="22px" src="{{ asset('icons/logout.svg') }}"
                                             alt="Logout">
                                        <span>Выйти</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                    <div class="tab-content">
                        @include('front.accounts.current-orders')
                        @include('front.accounts.order-history')
                        @include('front.accounts.personal-data')
                        @include('front.accounts.favorites')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('js/front.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.mask.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('input[data-double="phone"]').mask('+7 (000) 000 00 00')
        })
    </script>
@endsection