@extends('layouts.front.app')

@section('content')
    <section>
        <!-- START: BREADCRUMBS -->
        <div class="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumbs">
                        <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a
                                    href="{{ route('home') }}" itemprop="url">Главная</a></li>
                        <li class="sep">&gt;</li>
                        <li class="current">Доставка и оплата</li>
                    </ul><!-- .breadcrumbs -->
                </div>
            </div>
        </div>
        <!-- END: BREADCRUMBS -->
        <div class="container">
            <div class="row">
                <main class="col-xs-12 col-sm-9">
                    <div class="delivery-pay-wrapper">
                        <h1>Доставка и оплата</h1>

                        <div class="delivery-wrapper">
                            <h3>Условия доставки</h3>
                            <div>
                                <div><span>График работы:</span> доставка осуществляется <span>7 дней</span> в неделю.
                                </div>
                                <div><span>Сроки:</span> Возможна через <span>1-2 часа</span> после готовности заказа, и
                                    <span>3 часа</span> для индивидуального авторского заказа.
                                </div>
                            </div>
                            <div><span>Важно:</span> доставка до 08:00 оплачивается по двойному тарифу.</div>
                            <div class="delivery-cost">
                                <div><span>Стоимость доставки:</span></div>
                                @foreach($couriers as $courier)
                                    <div>{{ $courier->name }} -
                                        @if($courier->is_free == 0)
                                            <span>{{ round($courier->cost) . ' руб.'}}</span>
                                        @else
                                            <span>Бесплатно</span> {{ $courier->description ? '('. $courier->description . ')' : '' }}
                                            <a href="{{ session()->has('city') ? session()->get('city.map_href') : $defaulCity->map_href }}" target="_blank">Посмотреть на карте</a>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        {!! session()->has('city') ? session()->get('city.map') : $defaulCity->map !!}

                        <div class="pay-wrapper">
                            <h3>Способы оплаты</h3>
                            <div>
                                <div><span>Банковской картой при оформлении заказа через интернет-магазин:</span></div>
                                <div class="visa">Visa/Visa Electron</div>
                                <div class="mastercard">Mastercard</div>
                                <div class="maestro">Maestro</div>
                                <div class="mir">МИР</div>
                            </div>
{{--                            <div>--}}
{{--                                <div><span>Электронными деньгами и через платежные системы:</span></div>--}}
{{--                                <div class="paypal">PayPal</div>--}}
{{--                                <div class="yandex-money">Яндекс.Деньги</div>--}}
{{--                            </div>--}}
                            <div>
                                <div><span>Наличными:</span></div>
                                <div>При заборе заказа в нашей студии по адресу: {{ session()->has('city') ? session()->get('city.address') : $defaulCity->address }}
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </section>
@endsection