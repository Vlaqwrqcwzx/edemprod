<?php

use App\Shop\Cities\City;
use App\Shop\Couriers\Courier;
use Illuminate\Database\Seeder;

class CourierTableSeeder extends Seeder
{
    public function run()
    {
        $moscow = City::where('name', 'Москва')->first();

        Courier::firstOrCreate(
            [
                'name' => 'В пределах МКАД',
                'city_id' => $moscow->id,
            ],
            [
                'name' => 'В пределах МКАД',
                'description' => '',
                'is_free' => '0',
                'cost' => '400.00',
                'status' => '1',
                'city_id' => $moscow->id,
            ]
        );

        Courier::firstOrCreate(
            [
                'name' => 'За МКАД (до 10 км)',
                'city_id' => $moscow->id,
            ],
            [
                'name' => 'За МКАД (до 10 км)',
                'description' => '',
                'is_free' => '0',
                'cost' => '1000.00',
                'status' => '1',
                'city_id' => $moscow->id,
            ]
        );

        Courier::firstOrCreate(
            [
                'name' => 'Самовывоз',
                'city_id' => $moscow->id,
            ],
            [
                'name' => 'Самовывоз',
                'description' => 'Бесплатно (м. Братиславская, Новомарьинская ул., 12/12к1)',
                'is_free' => '1',
                'cost' => '0.00',
                'status' => '1',
                'city_id' => $moscow->id,
            ]
        );

        $tobolsk = City::where('name', 'Тобольск')->first();

        Courier::firstOrCreate(
            [
                'name' => 'В пределах города',
                'city_id' => $tobolsk->id,
            ],
            [
                'name' => 'В пределах города',
                'description' => '',
                'is_free' => '0',
                'cost' => '400.00',
                'status' => '1',
                'city_id' => $tobolsk->id,
            ]
        );

        Courier::firstOrCreate(
            [
                'name' => 'За город (до 10 км)',
                'city_id' => $tobolsk->id,
            ],
            [
                'name' => 'За город (до 10 км)',
                'description' => '',
                'is_free' => '0',
                'cost' => '1000.00',
                'status' => '1',
                'city_id' => $tobolsk->id,
            ]
        );

        Courier::firstOrCreate(
            [
                'name' => 'Самовывоз',
                'city_id' => $tobolsk->id,
            ],
            [
                'name' => 'Самовывоз',
                'description' => 'Зона ВУЗов, 1, Тобольск',
                'is_free' => '1',
                'cost' => '0.00',
                'status' => '1',
                'city_id' => $tobolsk->id,
            ]
        );
    }
}