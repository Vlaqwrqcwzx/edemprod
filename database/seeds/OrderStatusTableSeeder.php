<?php

use App\Shop\OrderStatuses\OrderStatus;
use Illuminate\Database\Seeder;

class OrderStatusTableSeeder extends Seeder
{
    public function run()
    {
        OrderStatus::firstOrCreate([
            'name'         => 'paid',
            'display_name' => 'оплачено',
            'color'        => 'green'
        ]);

        OrderStatus::firstOrCreate([
            'name'         => 'pending',
            'display_name' => 'ожидании оплаты',
            'color'        => 'orange'
        ]);

        OrderStatus::firstOrCreate([
            'name'         => 'error',
            'display_name' => 'ошибка',
            'color'        => 'red'
        ]);

        OrderStatus::firstOrCreate([
            'name'         => 'on-delivery',
            'display_name' => 'в процессе доставки',
            'color'        => 'blue'
        ]);

        OrderStatus::firstOrCreate([
            'name'         => 'ordered',
            'display_name' => 'завершенный заказ',
            'color'        => 'violet'
        ]);
    }
}