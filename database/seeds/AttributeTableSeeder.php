<?php

use App\Shop\Attributes\Attribute;
use App\Shop\AttributeValues\AttributeValue;
use Illuminate\Database\Seeder;

class AttributeTableSeeder extends Seeder
{
    public function run()
    {
        $sizeAttr = Attribute::firstOrCreate(['name' => 'Размеры']);

        AttributeValue::firstOrCreate([
            'name' => 'M (30-40 см)',
            'value' => '100',
            'attribute_id' => $sizeAttr->id
        ]);

        AttributeValue::firstOrCreate([
            'name' => 'L (45-50 см)',
            'value' => '133',
            'attribute_id' => $sizeAttr->id
        ]);

        AttributeValue::firstOrCreate([
            'name' => 'XL (55-70 см)',
            'value' => '188',
            'attribute_id' => $sizeAttr->id
        ]);

        $hashtagsAttr = Attribute::firstOrCreate(['name' => 'Hashtags']);

        AttributeValue::firstOrCreate([
            'name' => 'подсолнухи',
            'value' => 'podsolnukhi',
            'attribute_id' => $hashtagsAttr->id
        ]);

        AttributeValue::firstOrCreate([
            'name' => 'весеннийбукет',
            'value' => 'vesenniybuket',
            'attribute_id' => $hashtagsAttr->id
        ]);

        AttributeValue::firstOrCreate([
            'name' => 'яркийбукет',
            'value' => 'yarkiybuket',
            'attribute_id' => $hashtagsAttr->id
        ]);
    }
}