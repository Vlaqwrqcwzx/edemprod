<?php

use App\Shop\Brands\Brand;
use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    public function run()
    {
        Brand::firstOrCreate([
            'name' => 'Акция',
            'cover_icon' => 'ico-h-promo',
            'slug' => 'aktsiya'
        ]);

        Brand::firstOrCreate([
            'name' => 'Популярное',
            'cover_icon' => 'ico-h-featured',
            'slug' => 'populyarnoe'
        ]);

        Brand::firstOrCreate([
            'name' => 'К празднику',
            'cover_icon' => 'ico-h-box',
            'slug' => 'k-prazdniku'
        ]);
    }
}