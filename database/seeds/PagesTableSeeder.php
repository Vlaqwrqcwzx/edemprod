<?php

use App\Shop\Pages\Page;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::firstOrCreate(
            [
                'title' => 'О компании',
            ],
            [
                'slug' => str_slug('О компании'),
                'text' => ' '
            ]);

        Page::firstOrCreate(
            [
                'title' => 'Наши достижения',
            ],
            [
                'slug' => str_slug('Наши достижения'),
                'text' => ' '
            ]);

        Page::firstOrCreate(
            [
                'title' => 'СМИ о нас',
            ],
            [
                'slug' => str_slug('СМИ о нас'),
                'text' => ' '
            ]);

        Page::firstOrCreate(
            [
                'title' => 'Наши награды',
            ],
            [
                'slug' => str_slug('Наши награды'),
                'text' => ' '
            ]);

        Page::firstOrCreate(
            [
                'title' => 'Наши партнеры и клиенты',
            ],
            [
                'slug' => str_slug('Наши партнеры и клиенты'),
                'text' => ' '
            ]);

        Page::firstOrCreate(
            [
                'title' => 'Интерьер',
            ],
            [
                'slug' => str_slug('Интерьер'),
                'text' => ' '
            ]);

        Page::firstOrCreate(
            [
                'title' => 'Контакты',
            ],
            [
                'slug' => str_slug('Контакты'),
                'text' => ' '
            ]);

        Page::firstOrCreate(
            [
                'title' => 'Фотостудия',
            ],
            [
                'slug' => str_slug('Фотостудия'),
                'text' => ' '
            ]);

        Page::firstOrCreate(
            [
                'title' => 'Мастер классы',
            ],
            [
                'slug' => str_slug('Мастер классы'),
                'text' => ' '
            ]);

    }
}
