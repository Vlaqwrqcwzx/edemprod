<?php

use App\Shop\Cities\City;
use Illuminate\Database\Seeder;

class MyCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::firstOrCreate([
            'name' => 'Москва',
            'phone' => '8 (3456) 22-94-93',
            'address' => 'м. Братиславская, Новомарьинская ул., 12/12к1.',
        ]);

        City::firstOrCreate([
            'name' => 'Тобольск',
            'phone' => '8 (345) 622-93-93',
            'address' => 'Зона ВУЗов, 1, Тобольск',
        ]);
    }
}
