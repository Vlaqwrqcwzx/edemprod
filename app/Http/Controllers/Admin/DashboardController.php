<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $breadcumb = [
            ["name" => "Панель управления", "url" => route("admin.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Главная", "url" => route("admin.dashboard"), "icon" => "fa fa-home"],

        ];
        populate_breadcumb($breadcumb);
        return view('admin.dashboard');
    }
}
