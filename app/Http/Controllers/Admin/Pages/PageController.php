<?php

namespace App\Http\Controllers\Admin\Pages;

use App\Shop\Pages\Page;
use App\Shop\Pages\Requests\StorePageRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();
        return view('admin.pages.list', compact('pages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Page $page
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Page $page)
    {
        return view('admin.pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Page $page
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(StorePageRequest $request, Page $page)
    {
        $page->update($request->all());
        return redirect()->route('admin.pages.index');
    }

}
