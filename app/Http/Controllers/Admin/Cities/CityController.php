<?php

namespace App\Http\Controllers\Admin\Cities;

use App\Shop\Cities\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::all();
        return view('admin.cities.index', compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        City::create($request->all());
        return redirect()->route('admin.cities.index')->with('message', 'Город успешно добавлен');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param City $city
     * @return void
     */
    public function edit(City $city)
    {
        return view('admin.cities.edit', compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param City $city
     * @return void
     */
    public function update(Request $request, City $city)
    {
        $city->update($request->all());
        return redirect()->route('admin.cities.index')->with('message', 'Город успешно обновлен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param City $city
     * @return void
     */
    public function destroy(City $city)
    {
        $city->delete();
        return redirect()->route('admin.cities.index')->with('message', 'Город успешно удален');
    }
}
