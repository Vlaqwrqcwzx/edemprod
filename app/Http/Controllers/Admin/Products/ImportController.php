<?php

namespace App\Http\Controllers\Admin\Products;

use App\Shop\Attributes\Attribute;
use App\Shop\AttributeValues\AttributeValue;
use App\Shop\Categories\Category;
use App\Shop\ProductImages\ProductImage;
use App\Shop\Products\Product;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Sheets;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ImportController extends Controller
{
    private $client;

    private $productRepo;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepo = $productRepository;
    }

    public function index()
    {
        return view('admin.import.index');
    }

    public function setClientSettings()
    {
        $this->client = new Google_Client();
        $this->client->setApplicationName('Google Sheets and PHP');
        $this->client->setScopes([Google_Service_Sheets::SPREADSHEETS, Google_Service_Drive::DRIVE]);
        $this->client->setAccessType('offline');
        $this->client->setAuthConfig(public_path('google.json'));
    }

    public function getIdFromUrl($url) {
        preg_match('/[-\w]{25,}/', $url, $matches);
        return $matches[0];
    }

    public function execute(Request $request)
    {
        if (isset($request->address)) {
            $this->setClientSettings();
            $service = new Google_Service_Sheets($this->client);
            $driveService = new Google_Service_Drive($this->client);

            $response = $service->spreadsheets_values->get($request->address, "Лист1!A2:N100");
            $rows = $response->getValues();

            if (empty($rows)) {
                return redirect()->route('admin.import.index')->with('error', 'Таблица пуста');
            }

            foreach ($rows as $row) {

                $product = Product::updateOrCreate(
                    ['sku' => $row[1]],
                    [
                        'slug'        => str_slug($row[2]),
                        'sku'         => $row[1],
                        'name'        => $row[2],
                        'description' => $row[3],
                        'status'      => 1,
                        'quantity'    => (int)$row[5],
                        'is_sale'     => ($row[6] == 1) ? 1 : 0,
                        'price'       => (int)$row[7],
                        'sale_price'  => (int)$row[8],
                    ]
                );

                if (!empty($row[4])) {
                    $product->images()->delete();
                    $folderId = $this->getIdFromUrl($row[4]);
                    $optParams = [
                        'q' => "'" . $folderId . "' in parents",
                        'fields' => 'files(id, name)'
                    ];
                    $result = $driveService->files->listFiles($optParams);
                    $files = $result->getFiles();

                    if (count($files) > 0) {
                        foreach ($files as $k => $file) {
                            $content = $driveService->files->get($file->getId(), ['alt' => 'media']);
                            Storage::disk('public')->put('products/' . $file->getName(), $content->getBody()->getContents());
                            switch ($k) {
                                case 0:
                                    $product->cover = 'products/' . $file->getName();
                                    $product->save();
                                    break;
                                case 1:
                                    $product->cover_hover = 'products/' . $file->getName();
                                    $product->save();
                                    break;
                                default:
                                    $productImage = new ProductImage([
                                        'product_id' => $product->id,
                                        'src' => 'products/' . $file->getName()
                                    ]);
                                    $product->images()->save($productImage);
                            }
                        }
                    }
                }

                // привязка продукта в категории
                if (!empty($row[0])) {
                    $categories = explode(';', $row[0]);
                    $params = [];
                    foreach ($categories as $k => $category) {
                        $cat = Category::where('name', $category)->first();
                        if (isset($cat)) {
                            $params[$k] = $cat->id;
                        }
                    }
                    $product->categories()->sync($params);
                } else {
                    $product->detachCategories();
                }


                // обновление размеров и хэштегов
                $product->attributesValues()->detach();
                $attributes = [];
                if (!empty($row[9]))
                {
                    $sizes = explode(';', $row[9]);
                    foreach ($sizes as $size) {
                        if(!empty($size)) {
                            $attribute = AttributeValue::where('name', $size)->first();
                            if (isset($attribute)) {
                                $attributes[] = $attribute->id;
                            }
                        }
                    }
                }

                $hashtagAttribute = Attribute::where('name', 'Hashtags')->first();

                if (!empty($row[10]) && isset($hashtagAttribute))
                {
                    $hashtags = explode(';', $row[10]);
                    foreach ($hashtags as $tag) {
                        if (!empty($tag)) {
                            $attribute = AttributeValue::firstOrCreate(
                                ['name' => $tag],
                                [
                                    'name'         => $tag,
                                    'value'        => str_slug($tag),
                                    'attribute_id' => $hashtagAttribute->id
                                ]
                            );
                            $attributes[] = $attribute->id;
                        }
                    }
                }

                $product->attributesValues()->sync($attributes);

            }
            return redirect()->route('admin.import.index')->with('message', 'Имортирование прошло успешно');
        }
    }
}
