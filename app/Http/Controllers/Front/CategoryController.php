<?php

namespace App\Http\Controllers\Front;

use App\Shop\Brands\Repositories\BrandRepositoryInterface;
use App\Shop\Brands\Repositories\BrandRepository;
use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;
    private $brandRepo;

    /**
     * CategoryController constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository, BrandRepositoryInterface $brandRepo)
    {
        $this->categoryRepo = $categoryRepository;
        $this->brandRepo = $brandRepo;
    }

    /**
     * Find the category via the slug
     *
     * @param string $slug
     * @return \App\Shop\Categories\Category
     */
    public function getCategory(string $slug)
    {
        $category = $this->categoryRepo->findCategoryBySlug(['slug' => $slug]);

        $repo = new CategoryRepository($category);

        $products = $category->products()
            ->with('attributesValues')
            ->where('status', 1)
            ->paginate(12);

        $categories = $this->categoryRepo->listCategories('name', 'asc')->toTree();

        return view('front.categories.category', [
            'category' => $category,
            'products' => $products,
            'categories' => $categories
        ]);
    }

    public function getBrand(string $slug)
    {
        $brand = $this->brandRepo->findBrandBySlug(['slug' => $slug]);

        $products = $brand->products()
            ->with('attributesValues')
            ->where('status', 1)
            ->paginate(12);

        $categories = $this->categoryRepo->listCategories('name', 'asc')->toTree();

        return view('front.categories.category', [
            'category' => $brand,
            'products' => $products,
            'categories' => $categories
        ]);
    }
}
