<?php

namespace App\Http\Controllers\Front;

use App\Shop\AttributeValues\AttributeValue;
use App\Shop\Carts\Requests\AddToCartRequest;
use App\Shop\Carts\Requests\UpdateCartRequest;
use App\Shop\Carts\Repositories\Interfaces\CartRepositoryInterface;
use App\Shop\Cities\City;
use App\Shop\Coupons\Coupon;
use App\Shop\Couriers\Courier;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\ProductAttributes\Repositories\ProductAttributeRepositoryInterface;
use App\Shop\Products\Product;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Products\Repositories\ProductRepository;
use App\Shop\Products\Transformations\ProductTransformable;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\CartItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    use ProductTransformable;

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepo;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepo;

    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepo;

    /**
     * @var ProductAttributeRepositoryInterface
     */
    private $productAttributeRepo;

    /**
     * CartController constructor.
     * @param CartRepositoryInterface $cartRepository
     * @param ProductRepositoryInterface $productRepository
     * @param CourierRepositoryInterface $courierRepository
     * @param ProductAttributeRepositoryInterface $productAttributeRepository
     */
    public function __construct(
        CartRepositoryInterface $cartRepository,
        ProductRepositoryInterface $productRepository,
        CourierRepositoryInterface $courierRepository,
        ProductAttributeRepositoryInterface $productAttributeRepository
    )
    {
        $this->cartRepo = $cartRepository;
        $this->productRepo = $productRepository;
        $this->courierRepo = $courierRepository;
        $this->productAttributeRepo = $productAttributeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $defaultCourier = request()->session()->has('courierId')
            ? $this->courierRepo->findCourierById(request()->session()->get('courierId'))
            : Courier::current()->first();
        $shippingFee = isset($defaultCourier) ? $this->cartRepo->getShippingFee($defaultCourier) : 0; // цена доставки
        $subTotal = $this->cartRepo->getTotal(); // цена товаров
        $postCard = $this->cartRepo->getTotalPostCard(); // цена открыток
        $countItems = $this->cartRepo->countItems();
        $couponDiscount = $this->cartRepo->getTotalCouponDiscount();

        return view('front.carts.cart', [
            'shippingFee' => $shippingFee,
            'postCard' => $postCard,
            'countItems' => $countItems,
            'couponDiscount' => $couponDiscount,
            'subTotal' => $subTotal,
            'total' => $shippingFee + $postCard + $subTotal - $couponDiscount,
            'cartItems' => $this->cartRepo->getCartItemsTransformed(),
            'couriers' => Courier::current()->get(),
            'customer' => Auth::user(),
            'defaultCourier' => $defaultCourier
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddToCartRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddToCartRequest $request)
    {
        $product = $this->productRepo->findProductById($request->input('product'));

        // размер
        $options = [];
        if ($request->has('productAttribute')) {
            $attr = AttributeValue::find($request->input('productAttribute'));
        }
        if (!isset($attr))
            $attr = AttributeValue::where('value', '100')->first();

        $options['product_attribute'] = $attr;

        // настройка полей для открытки
        $options['usePostCard'] = null;
        $options['postCardPrice'] = 100;
        $options['postCardText'] = '';

        // цена
        if ($product->is_sale == 1) {
            $product->price = ($product->sale_price * $attr->value) / 100;
        } else {
            $product->price = ($product->price * $attr->value) / 100;
        }

        $this->cartRepo->addToCart($product, $request->input('quantity'), $options);

        return redirect()->route('cart.index')
            ->with('message', 'Товар успешно добавлен в корзину');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCartRequest $request, $id)
    {
        $this->cartRepo->updateQuantityInCart($id, $request->input('quantity'));

        return [
            'product' => $this->cartRepo->findItem($id),
            'totalPrices' => $this->getTotalPrice(),
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->cartRepo->removeToCart($id);

        request()->session()->flash('message', 'Товар удален с корзины');
        return redirect()->route('cart.index');
    }

    public function clear()
    {
        $this->cartRepo->clearCart();
        return redirect()->route('cart.index');
    }

    public function usePostCard($id)
    {
        $item = $this->cartRepo->findItem($id);
        if (is_null($item->options['usePostCard']))
            $item->options['usePostCard'] = 'use';
        else
            $item->options['usePostCard'] = null;

        return $this->getTotalPrice();
    }

    public function getTotalPrice()
    {
        $defaultCourier = $this->courierRepo->findCourierById(request()->session()->get('courierId', 1));
        $shippingFee = $this->cartRepo->getShippingFee($defaultCourier); // цена доставки
        $subTotal = $this->cartRepo->getTotal(); // цена товаров
        $postCard = $this->cartRepo->getTotalPostCard(); // цена открыток
        $countItems = $this->cartRepo->countItems();
        $couponDiscount = $this->cartRepo->getTotalCouponDiscount();

        return [
            'shippingFee' => round($shippingFee),
            'postCard' => $postCard,
            'countItems' => $countItems,
            'couponDiscount' => round($couponDiscount),
            'subTotal' => round($subTotal),
            'total' => round($shippingFee + $postCard + $subTotal - $couponDiscount),
        ];

    }

    public function courierChange($id)
    {
        request()->session()->put('courierId', $id);
        return $this->getTotalPrice();
    }

    public function setCoupon(string $code)
    {
        if (!empty($code)) {
            $coupon = Coupon::where('code', $code)
                ->where('startDate', '<', Carbon::now())
                ->where('endDate', '>', Carbon::now())
                ->where('count', '>', '0')
                ->first();

            if (isset($coupon)) {
                session()->put('coupon', $coupon);
                $coupon->count--;
                $coupon->save();
                $result = [
                    'total' => $this->getTotalPrice(),
                    'coupon' => $coupon
                ];
                return response()->json(['result' => $result]);
            }
        }
        return response()->json(['message' => 'error']);
    }

    public function destroyCoupon()
    {
        if (session()->has('coupon')) {
            session()->remove('coupon');
        }
        return response()->json(['message' => 'ok']);
    }

    public function setWishes(Request $request)
    {
        $item = $this->cartRepo->findItem($request->rowId);
        $item->options['postCardText'] = $request->text;
        return $item;
    }

    public function deferred()
    {
        $countItems = $this->cartRepo->countItems();
        return view('front.carts.deferred', [
            'customer' => Auth::user(),
            'countItems' => $countItems
        ]);
    }
}
