<?php

namespace App\Http\Controllers\Front;

use App\Shop\Brands\Brand;
use App\Shop\Categories\Category;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\Cities\City;
use App\Shop\Couriers\Courier;
use App\Shop\Favorites\Favorites;
use App\Shop\Pages\Page;
use App\Shop\Products\Product;
use http\Env\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

    /**
     * HomeController constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepo = $categoryRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $mostFresh = Product::with('images', 'brand', 'attributesValues', 'categories')
            ->latest()
            ->limit(3)
            ->enabled()
            ->get();

        $brandsWithLastProduct = Brand::with('lastProduct')->get();

        return view('front.index', compact('mostFresh', 'brandsWithLastProduct'));
    }

    public function addToFavorite(Product $product)
    {
        if (Auth::guest()) {
            return response()->json(['message' => 'error']);
        }
        $addYet = Favorites::where([
            'products_id' => $product->id,
            'customers_id' => Auth::user()->id,
        ])->first();
        if (!isset($addYet)) {
            Favorites::create([
                'products_id' => $product->id,
                'customers_id' => Auth::user()->id,
            ]);
            return response()->json(['message' => 'success']);
        } else {
            $addYet->delete();
            return response()->json(['message' => 'deleted']);
        }
    }

    public function delivery()
    {
        $couriers = Courier::current()->get();
        return view('front.delivery-and-pay', compact('couriers'));
    }

    /**
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param Page $page
     */
    public function show(string $slug)
    {
        $page = Page::where('slug', $slug)->first();
        return view('front.pages.index', compact('page'));
    }

    public function changeCity(City $city)
    {
        Session::put('city', $city);
        return redirect()->back();
    }
}
