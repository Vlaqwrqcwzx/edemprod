<?php

namespace App\Http\Controllers\Front;

use App\Events\OrderCreateEvent;
use App\Shop\Addresses\Repositories\Interfaces\AddressRepositoryInterface;
use App\Shop\Cart\Requests\CartCheckoutRequest;
use App\Shop\Carts\Repositories\Interfaces\CartRepositoryInterface;
use App\Shop\Carts\Requests\PayPalCheckoutExecutionRequest;
use App\Shop\Carts\Requests\StripeExecutionRequest;
use App\Shop\Checkout\CheckoutRepository;
use App\Shop\Cities\City;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Customers\Customer;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Shop\Orders\Order;
use App\Shop\Orders\Repositories\Interfaces\OrderRepositoryInterface;
use App\Shop\Orders\Repositories\OrderRepository;
use App\Shop\OrderStatuses\OrderStatus;
use App\Shop\OrderStatuses\Repositories\OrderStatusRepository;
use App\Shop\PaymentMethods\Paypal\Exceptions\PaypalRequestError;
use App\Shop\PaymentMethods\Paypal\Repositories\PayPalExpressCheckoutRepository;
use App\Shop\PaymentMethods\Stripe\Exceptions\StripeChargingErrorException;
use App\Shop\PaymentMethods\Stripe\StripeRepository;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Products\Transformations\ProductTransformable;
use App\Shop\Shipping\ShippingInterface;
use Carbon\Carbon;
use Exception;
use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use PayPal\Exception\PayPalConnectionException;
use Ramsey\Uuid\Uuid;

class CheckoutController extends Controller
{
    use ProductTransformable;

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepo;

    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepo;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepo;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepo;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepo;

    /**
     * @var PayPalExpressCheckoutRepository
     */
    private $payPal;

    /**
     * @var ShippingInterface
     */
    private $shippingRepo;

    public function __construct(
        CartRepositoryInterface $cartRepository,
        CourierRepositoryInterface $courierRepository,
        AddressRepositoryInterface $addressRepository,
        CustomerRepositoryInterface $customerRepository,
        ProductRepositoryInterface $productRepository,
        OrderRepositoryInterface $orderRepository,
        ShippingInterface $shipping
    )
    {
        $this->cartRepo = $cartRepository;
        $this->courierRepo = $courierRepository;
        $this->addressRepo = $addressRepository;
        $this->customerRepo = $customerRepository;
        $this->productRepo = $productRepository;
        $this->orderRepo = $orderRepository;
        $this->payPal = new PayPalExpressCheckoutRepository;
        $this->shippingRepo = $shipping;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = $this->cartRepo->getCartItems();
        $customer = $request->user();
        $rates = null;
        $shipment_object_id = null;

        if (env('ACTIVATE_SHIPPING') == 1) {
            $shipment = $this->createShippingProcess($customer, $products);
            if (!is_null($shipment)) {
                $shipment_object_id = $shipment->object_id;
                $rates = $shipment->rates;
            }
        }

        // Get payment gateways
        $paymentGateways = collect(explode(',', config('payees.name')))->transform(function ($name) {
            return config($name);
        })->all();

        $billingAddress = $customer->addresses()->first();

        return view('front.checkout', [
            'customer' => $customer,
            'billingAddress' => $billingAddress,
            'addresses' => $customer->addresses()->get(),
            'products' => $this->cartRepo->getCartItems(),
            'subtotal' => $this->cartRepo->getSubTotal(),
            'tax' => $this->cartRepo->getTax(),
            'total' => $this->cartRepo->getTotal(2),
            'payments' => $paymentGateways,
            'cartItems' => $this->cartRepo->getCartItemsTransformed(),
            'shipment_object_id' => $shipment_object_id,
            'rates' => $rates
        ]);
    }

    /**
     * Checkout the items
     *
     * @param CartCheckoutRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Shop\Addresses\Exceptions\AddressNotFoundException
     * @throws \App\Shop\Customers\Exceptions\CustomerPaymentChargingErrorException
     * @codeCoverageIgnore
     */
    public function store(CartCheckoutRequest $request)
    {
        // создаем адрес доставки
        $request['customer_id'] = auth()->user()->id;
        $defaultCity = City::first();
        $request['city_id'] = session()->has('city') ? session()->get('city.id') : $defaultCity->id;
        $address = $this->addressRepo->createAddress($request->except('_token', '_method'));

        // статус заказа
        $orderStatusRepo = new OrderStatusRepository(new OrderStatus);
        $os = $orderStatusRepo->findOrderStatusById(2);

        // формирование заказа
        $defaultCourier = $this->courierRepo->findCourierById(request()->session()->get('courierId', 1));
        $shippingFee = $this->cartRepo->getShippingFee($defaultCourier); // цена доставки
        $subTotal = $this->cartRepo->getTotal(); // цена товаров
        $postCard = $this->cartRepo->getTotalPostCard(); // цена открыток
        $couponDiscount = $this->cartRepo->getTotalCouponDiscount(); // скидка

        $checkoutRepo = new CheckoutRepository;
        $order = $checkoutRepo->buildCheckoutItems([
            'courier_id' => $defaultCourier->id,
            'customer_id' => $request['customer_id'],
            'address_id' => $address->id,
            'order_status_id' => $os->id,
            'payment' => strtolower(config('bank-transfer.name')),
            'total_shipping' => $shippingFee,
            'total_postcards' => $postCard,
            'coupon_id' => session()->has('coupon') ? session()->get('coupon.id') : null,
            'discounts' => $couponDiscount,
            'total_products' => $subTotal,
            'total' => $shippingFee + $postCard + $subTotal - $couponDiscount,
            'shipping_date' => Carbon::parse($request->date),
            'shipping_time' => $request->time,
            'comment' => $request->comment,
        ]);

        try {
            $vars = array();

            $vars['userName'] = 'edemsad-api';
            $vars['password'] = 'edemsad';

            /* ID заказа в магазине */
            $vars['orderNumber'] = $order->id;

            /* Сумма заказа в копейках */
            $vars['amount'] = $order->total * 100;

            /* URL куда клиент вернется в случае успешной оплаты */
            $vars['returnUrl'] = route('checkout.success');

            /* URL куда клиент вернется в случае ошибки */
            $vars['failUrl'] = route('checkout.cancel');

            /* Описание заказа, не более 24 символов, запрещены % + \r \n */
            $vars['description'] = 'Заказ №' . $order->id . ' на edem.ru';

            $ch = curl_init('https://3dsec.sberbank.ru/payment/rest/register.do?' . http_build_query($vars));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
            $res = curl_exec($ch);
            curl_close($ch);

            if ($res) {
                $response = json_decode($res, true);

                if ($response['orderId'] && $response['formUrl']) {
                    $order->update([
                        'reference' => $response['orderId']
                    ]);
                    return redirect($response['formUrl']);
                }

            }
        } catch (Exception $e) {
            Log::info($e->getMessage());
            return redirect()->route('cart.index')->with('error', 'Возникла проблема при обработке вашего запроса.');
        }

//        switch ($request->input('payment')) {
//            case 'paypal':
//                return $this->payPal->process($shippingFee, $request);
//                break;
//            case 'stripe':
//
//                $details = [
//                    'description' => 'Stripe payment',
//                    'metadata' => $this->cartRepo->getCartItems()->all()
//                ];
//
//                $customer = $this->customerRepo->findCustomerById(auth()->id());
//                $customerRepo = new CustomerRepository($customer);
//                $customerRepo->charge($this->cartRepo->getTotal(2, $shippingFee), $details);
//                break;
//            default:
//        }
    }

    /**
     * Execute the PayPal payment
     *
     * @param PayPalCheckoutExecutionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function executePayPalPayment(PayPalCheckoutExecutionRequest $request)
    {
        try {
            $this->payPal->execute($request);
            $this->cartRepo->clearCart();

            return redirect()->route('checkout.success');
        } catch (PayPalConnectionException $e) {
            throw new PaypalRequestError($e->getData());
        } catch (Exception $e) {
            throw new PaypalRequestError($e->getMessage());
        }
    }

    /**
     * @param StripeExecutionRequest $request
     * @return \Stripe\Charge
     */
    public function charge(StripeExecutionRequest $request)
    {
        try {
            $customer = $this->customerRepo->findCustomerById(auth()->id());
            $stripeRepo = new StripeRepository($customer);

            $stripeRepo->execute(
                $request->all(),
                Cart::total(),
                Cart::tax()
            );
            return redirect()->route('checkout.success')->with('message', 'Stripe payment successful!');
        } catch (StripeChargingErrorException $e) {
            Log::info($e->getMessage());
            return redirect()->route('checkout.index')->with('error', 'There is a problem processing your request.');
        }
    }

    /**
     * Cancel page
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cancel(Request $request)
    {
        return view('front.checkout-cancel', ['data' => $request->all()]);
    }

    /**
     * Success page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function success()
    {
        $vars = array();
        $vars['userName'] = 'edemsad-api';
        $vars['password'] = 'edemsad';
        $vars['orderId'] = request()->get('orderId');

        $ch = curl_init('https://3dsec.sberbank.ru/payment/rest/getOrderStatusExtended.do?' . http_build_query($vars));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $res = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($res, JSON_OBJECT_AS_ARRAY);

        if(isset($res['errorMessage'])){
            if($res['orderStatus'] == 2 ){
                $order = Order::where('reference', request()->get('orderId'))->first();
                $order->update([
                    'order_status_id' => 1
                ]);

                // отправка Mail клиенту
                event(new OrderCreateEvent($order));

                session()->remove('coupon');
                Cart::destroy();
                return redirect()->route('accounts', ['tab' => 'current'])->with('message', "Спасибо, заказ №".$order->id." оплачен.");
            }
        }
        return redirect()->route('cart.index')->with('error', 'Возникла проблема при обработке вашего запроса.');

    }

    /**
     * @param Customer $customer
     * @param Collection $products
     *
     * @return mixed
     */
    private function createShippingProcess(Customer $customer, Collection $products)
    {
        $customerRepo = new CustomerRepository($customer);

        if ($customerRepo->findAddresses()->count() > 0 && $products->count() > 0) {

            $this->shippingRepo->setPickupAddress();
            $deliveryAddress = $customerRepo->findAddresses()->first();
            $this->shippingRepo->setDeliveryAddress($deliveryAddress);
            $this->shippingRepo->readyParcel($this->cartRepo->getCartItems());

            return $this->shippingRepo->readyShipment();
        }
    }

}
