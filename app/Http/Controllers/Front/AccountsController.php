<?php

namespace App\Http\Controllers\Front;

use App\Rules\MatchOldPassword;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Customers\Customer;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Favorites\Favorites;
use App\Shop\Orders\Order;
use App\Shop\Orders\Transformers\OrderTransformable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AccountsController extends Controller
{
    use OrderTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepo;

    /**
     * AccountsController constructor.
     *
     * @param CourierRepositoryInterface $courierRepository
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        CourierRepositoryInterface $courierRepository,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->customerRepo = $customerRepository;
        $this->courierRepo = $courierRepository;
    }

    public function index()
    {
        $customer = $this->customerRepo->findCustomerById(auth()->user()->id);

        $customerRepo = new CustomerRepository($customer);
        $orders = $customerRepo->findOrders(['*'], 'created_at');

        $orders->transform(function (Order $order) {
            return $this->transformOrder($order);
        });

        $orders->load('products', 'products.attributesValues');
        $favorites = $customer->favorites()->with('attributesValues')->get();

        return view('front.accounts', [
            'customer' => $customer,
            'orders' => $this->customerRepo->paginateArrayResults($orders->toArray(), 15),
            'favorites' => $favorites
        ]);
    }

    public function changePassword(Request $request)
    {
        $messages = [
            'required' => 'Поля для смены пароля обязательны к заполнению',
            'same:new_password' => 'Вы не правильно повторили новый пароль',
        ];
        $validator = Validator::make($request->all(), [
            'password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['required', 'same:new_password'],
        ], $messages);

        if ($validator->fails()) {
            return redirect()->route('accounts', ['tab' => 'personal'])->withErrors($validator);
        }

        Customer::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);

        return redirect()->route('accounts', ['tab' => 'personal'])
            ->with('message', 'Пароль успешно изменен');
    }

    public function update(Request $request, Customer $customer)
    {
        $customer->update($request->all());
        return redirect()->route('accounts', ['tab' => 'personal'])
            ->with('message', 'Контактные данные успешно изменены');
    }

    public function clearFavorites()
    {
        $customer = $this->customerRepo->findCustomerById(auth()->user()->id);
        $customer->favorites()->detach();
        return redirect()->route('accounts', ['tab' => 'favorites'])
            ->with('message', 'Все товары удалены из избранного');
    }

}
