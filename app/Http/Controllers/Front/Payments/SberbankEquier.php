<?php

namespace App\Http\Controllers\Front\Payments;

use App\Shop\Orders\Order;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Log;

class SberbankEquier extends Equier
{
    private $login;

    private $pass;

    private $endpoint;

    protected $client;

    const ENDPOINTS = [
        'register' 	=> 	'register.do',
        'status' 	=> 	'getOrderStatusExtended.do'
    ];

    public function __construct()
    {
        $this->baseUri = env('SBERBANK_EQUIRING_BASE_URI');
        $this->params['userName'] = env('SBERBANK_EQUIRING_LOGIN');
        $this->params['password'] = env('SBERBANK_EQUIRING_PASS' );
        $this->params['returnUrl'] = env('SBERBANK_EQUIRING_RETURN_URL');
        $this->setClient();
    }
    /**
     * Отправляет REST запрос для полчения информации о статусе оплаты заказа
     *
     */
    public function getPaymentStatus($orderId)
    {
        $endpoint = $this->getEndpoint('status');

        $params = [
            'userName' 	=> 	$this->params['userName'],
            'password'	=>  $this->params['password'],
            'orderId' 	=>	$orderId
        ];



        try {
            Log::debug('Параметры запроса эквайеру (статус заказа).', $params );
            $response = $this->client->request( 'GET', $endpoint, [ 'query' => $params ] );
            if( $response->getStatusCode() == 200 ) {
                dd($response);
                Log::debug('Ответ от эквайринга (статус заказа).', json_decode( $response->getBody() , true ) );
                return json_decode( $response->getBody() , true );
            }
            else return false;

        } catch (ConnectException $exception) {
            //Log::error('Ошибка входа через СБОЛ.');
            throw $exception;
        } catch (ClientException $exception) {
            //report($exception);
            $response = $exception->getResponse();
        }
    }

    /**
     * Отправляет REST запрос для регистрации заказа в системе эквайринга Сбербанк и обрабатывает его
     *
     */
    public function registerPayment(Order $order)
    {
        $endpoint = $this->getEndpoint('register');

        $params   = array_merge(
            $this->params,
            [
                'amount' => $order->total * 100,
                'orderNumber' => $order->id,
                'description' => 'Заказ №' . $order->id . ' на edem.ru'
            ]
        );

        try {
            Log::debug('Параметры запроса эквайеру.', $params );
            $response = $this->client->request( 'GET', $endpoint, [ 'query' => $params ] );
            if( $response->getStatusCode() == 200 ) {
                Log::debug('Ответ от эквайринга.', json_decode( $response->getBody() , true ) );
                return json_decode( $response->getBody() , true );
            }
            else return false;

        } catch (ConnectException $exception) {
            //Log::error('Ошибка входа через СБОЛ.');
            throw $exception;
        } catch (ClientException $exception) {
            //report($exception);
            $response = $exception->getResponse();
        }
    }


    private function getEndpoint( $key )
    {
        return $this->baseUri . self::ENDPOINTS[$key];
    }
}