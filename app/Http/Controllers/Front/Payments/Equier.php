<?php

namespace App\Http\Controllers\Front\Payments;

use GuzzleHttp\Client;

abstract class Equier
{
    protected function setClient()
    {
        $this->client = new Client([
            'base_uri' => $this->baseUri,
        ]);
    }
}