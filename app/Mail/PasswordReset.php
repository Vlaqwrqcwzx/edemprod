<?php

namespace App\Mail;

use App\Shop\Customers\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordReset extends Mailable
{
    use Queueable, SerializesModels;

    protected $customer;
    protected $token;

    /**
     * Create a new message instance.
     *
     * @param Customer $customer
     */
    public function __construct(Customer $customer, $token)
    {
        $this->subject = 'Сброс пароля в центре флористики "Эдемский сад"';
        $this->customer = $customer;
        $this->token = $token;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'customer' => $this->customer,
            'token' => $this->token
        ];
        return $this->view('emails.customer.password-reset', $data);
    }
}
