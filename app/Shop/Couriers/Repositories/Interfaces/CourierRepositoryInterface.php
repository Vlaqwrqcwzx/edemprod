<?php

namespace App\Shop\Couriers\Repositories\Interfaces;

use Illuminate\Http\UploadedFile;
use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Shop\Couriers\Courier;
use Illuminate\Support\Collection;

interface CourierRepositoryInterface extends BaseRepositoryInterface
{
    public function createCourier(array $data) : Courier;

    public function updateCourier(array $params) : bool;

    public function findCourierById(int $id) : Courier;

    public function saveCoverImage(UploadedFile $file) : string;

    public function listCouriers(string $order = 'id', string $sort = 'desc') : Collection;

    public function deleteCourier();
}
