<?php

namespace App\Shop\Couriers;

use App\Shop\Cities\City;
use App\Shop\Orders\Order;
use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'url',
        'is_free',
        'cost',
        'status',
        'image',
        'city_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function scopeCurrent($query)
    {
        $defaulCity =  City::first();
        $city_id = session()->has('city') ? session()->get('city.id') : $defaulCity->id;
        return $query->where('city_id', $city_id);
    }

    public function isFree()
    {
        return ($this->is_free == 1);
    }
}
