<?php

namespace App\Shop\Pages\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'text' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Поле "Название " обязательно для заполнения',
            'text.required'  => 'Поле "Описание" обязательно для заполнения'
        ];
    }
}
