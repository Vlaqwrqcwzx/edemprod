<?php

namespace App\Shop\Addresses\Transformations;

use App\Shop\Addresses\Address;
use App\Shop\Cities\Repositories\CityRepository;
use App\Shop\Countries\Repositories\CountryRepository;
use App\Shop\Customers\Customer;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Provinces\Province;
use App\Shop\Provinces\Repositories\ProvinceRepository;
use App\Shop\Cities\City;
use App\Shop\Countries\Country;

trait AddressTransformable
{
    /**
     * Transform the address
     *
     * @param Address $address
     *
     * @return Address
     * @throws \App\Shop\Cities\Exceptions\CityNotFoundException
     * @throws \App\Shop\Countries\Exceptions\CountryNotFoundException
     * @throws \App\Shop\Customers\Exceptions\CustomerNotFoundException
     */
    public function transformAddress(Address $address)
    {
        if (isset($address->city)) {
            $address->city = City::find($address->city);
        }
        return $address;
    }
}
