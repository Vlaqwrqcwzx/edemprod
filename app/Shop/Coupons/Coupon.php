<?php

namespace App\Shop\Coupons;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = ['code', 'startDate', 'endDate', 'count', 'caption', 'sale_percent'];

    protected $dates = ['startDate', 'endDate'];
}
