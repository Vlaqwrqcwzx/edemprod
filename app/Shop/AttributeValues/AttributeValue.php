<?php

namespace App\Shop\AttributeValues;

use App\Shop\Attributes\Attribute;
use App\Shop\ProductAttributes\ProductAttribute;
use Illuminate\Database\Eloquent\Model;

class AttributeValue extends Model
{
    protected $fillable = [
        'name',
        'value',
        'attribute_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }

}
