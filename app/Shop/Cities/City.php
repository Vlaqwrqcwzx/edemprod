<?php

namespace App\Shop\Cities;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'address',
        'map_href',
        'map'
    ];

    public $timestamps = false;

}
