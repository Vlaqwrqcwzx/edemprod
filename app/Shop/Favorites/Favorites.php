<?php

namespace App\Shop\Favorites;

use Illuminate\Database\Eloquent\Model;

class Favorites extends Model
{
    protected $fillable = [
        'products_id',
        'customers_id'
    ];

    protected $table = 'favorites';
}
