"use strict";
var multiItemSlider = (function () {
    return function (selector, config) {
        var _mainElements = document.querySelectorAll(selector); // РѕСЃРЅРѕРІРЅС‹Р№ СЌР»РµРјРµРЅС‚ Р±Р»РѕРєР°
        _mainElements.forEach((_mainElement) => {
            var _sliderWrapper = _mainElement.querySelector(".slider__wrapper"), // РѕР±РµСЂС‚РєР° РґР»СЏ .slider-item
            _sliderItems = _mainElement.querySelectorAll(".slider__item"), // СЌР»РµРјРµРЅС‚С‹ (.slider-item)
            _sliderControls = _mainElement.querySelectorAll(".slider__control"), // СЌР»РµРјРµРЅС‚С‹ СѓРїСЂР°РІР»РµРЅРёСЏ
            _sliderControlLeft = _mainElement.querySelector(
                ".slider__control_left"
            ), // РєРЅРѕРїРєР° "LEFT"
            _sliderControlRight = _mainElement.querySelector(
                ".slider__control_right"
            ), // РєРЅРѕРїРєР° "RIGHT"
            _wrapperWidth = parseFloat(getComputedStyle(_sliderWrapper).width), // С€РёСЂРёРЅР° РѕР±С‘СЂС‚РєРё
            _itemWidth = parseFloat(getComputedStyle(_sliderItems[0]).width), // С€РёСЂРёРЅР° РѕРґРЅРѕРіРѕ СЌР»РµРјРµРЅС‚Р°
            _positionLeftItem = 0, // РїРѕР·РёС†РёСЏ Р»РµРІРѕРіРѕ Р°РєС‚РёРІРЅРѕРіРѕ СЌР»РµРјРµРЅС‚Р°
            _transform = 0, // Р·РЅР°С‡РµРЅРёРµ С‚СЂР°РЅС„СЃРѕС„СЂРјР°С†РёРё .slider_wrapper
            _step = (_itemWidth / _wrapperWidth) * 100, // РІРµР»РёС‡РёРЅР° С€Р°РіР° (РґР»СЏ С‚СЂР°РЅСЃС„РѕСЂРјР°С†РёРё)
            _items = []; // РјР°СЃСЃРёРІ СЌР»РµРјРµРЅС‚РѕРІ
        // РЅР°РїРѕР»РЅРµРЅРёРµ РјР°СЃСЃРёРІР° _items
        _sliderItems.forEach(function (item, index) {
            _items.push({ item: item, position: index, transform: 0 });
        });

        if (_mainElement.querySelectorAll(".slider__indicators").length) {
            var _sliderIndicators = _mainElement.querySelector(
                ".slider__indicator-active"
                ), // Р°РєС‚РёРІРЅС‹Р№ РёРЅРґРёРєР°С‚РѕСЂ
                _indicatorTransform = 0, // Р·РЅР°С‡РµРЅРёРµ С‚СЂР°РЅС„СЃРѕС„СЂРјР°С†РёРё .slider__indicator-active
                _indicatorStep = 20; // РІРµР»РёС‡РёРЅР° С€Р°РіР° (РґР»СЏ С‚СЂР°РЅСЃС„РѕСЂРјР°С†РёРё РёРЅРґРёРєР°С‚РѕСЂР°)
        }

        var position = {
            getMin: 0,
            getMax: _items.length - 1,
        };

        var _transformItem = function (direction) {
            if (direction === "right") {
                if (
                    _positionLeftItem + _wrapperWidth / _itemWidth - 1 >=
                    position.getMax
                ) {
                    return;
                }
                if (
                    _sliderControlLeft.classList.contains(
                        "slider__control_left-inactive"
                    )
                ) {
                    _sliderControlLeft.classList.add("slider__control_left-active");
                    _sliderControlLeft.classList.remove(
                        "slider__control_left-inactive"
                    );
                }
                if (
                    _sliderControlRight.classList.contains(
                        "slider__control_right-active"
                    ) &&
                    _positionLeftItem + _wrapperWidth / _itemWidth >= position.getMax
                ) {
                    _sliderControlRight.classList.remove(
                        "slider__control_right-active"
                    );
                    _sliderControlRight.classList.add("slider__control_right-inactive");
                }
                if (_sliderIndicators) _indicatorTransform += _indicatorStep;
                _positionLeftItem++;
                _transform -= _step;
            }
            if (direction === "left") {
                if (_positionLeftItem <= position.getMin) {
                    return;
                }
                if (
                    _sliderControlRight.classList.contains(
                        "slider__control_right-inactive"
                    )
                ) {
                    _sliderControlRight.classList.add("slider__control_right-active");
                    _sliderControlRight.classList.remove(
                        "slider__control_right-inactive"
                    );
                }
                if (
                    _sliderControlLeft.classList.contains(
                        "slider__control_left-active"
                    ) &&
                    _positionLeftItem - 1 <= position.getMin
                ) {
                    _sliderControlLeft.classList.remove("slider__control_left-active");
                    _sliderControlLeft.classList.add("slider__control_left-inactive");
                }
                if (_sliderIndicators) _indicatorTransform -= _indicatorStep;

                _positionLeftItem--;
                _transform += _step;
            }
            if (_sliderIndicators)
                _sliderIndicators.style.transform =
                    "translateX(" + _indicatorTransform + "px)";
            _sliderWrapper.style.transform = "translateX(" + _transform + "%)";
        };

        // РѕР±СЂР°Р±РѕС‚С‡РёРє СЃРѕР±С‹С‚РёСЏ click РґР»СЏ РєРЅРѕРїРѕРє "РЅР°Р·Р°Рґ" Рё "РІРїРµСЂРµРґ"
        var _controlClick = function (e) {
            if (e.target.classList.contains("slider__control")) {
                e.preventDefault();
                var direction = e.target.classList.contains("slider__control_right")
                    ? "right"
                    : "left";
                _transformItem(direction);
            }
        };

        var _setUpListeners = function () {
            // РґРѕР±Р°РІР»РµРЅРёРµ Рє РєРЅРѕРїРєР°Рј "РЅР°Р·Р°Рґ" Рё "РІРїРµСЂРµРґ" РѕР±СЂР±РѕС‚С‡РёРєР° _controlClick РґР»СЏ СЃРѕР±С‹С‚СЏ click
            _sliderControls.forEach(function (item) {
                item.addEventListener("click", _controlClick);
            });
        };

        // РёРЅРёС†РёР°Р»РёР·Р°С†РёСЏ
        _setUpListeners();

        return {
            right: function () {
                // РјРµС‚РѕРґ right
                _transformItem("right");
            },
            left: function () {
                // РјРµС‚РѕРґ left
                _transformItem("left");
            },
        };
    });
    };
})();

var slider = multiItemSlider(".slider");