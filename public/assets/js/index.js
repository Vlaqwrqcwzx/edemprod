var Coupons = {
    couponDestroy : function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url:"destroyCoupon",
            method:"POST",
            success: function(data) {
                if (data.message == 'ok') {
                    toastr.info('Купон удален');
                    $('#coupon').val('');
                    $('.coupon-success').empty();
                    $('.coupon-caption').val('');
                    $('.discount-block').empty();
                    $('.discount-block-left').empty();
                    $('#coupon-buttons').html('<button class="btn btn-primary custom-button rich-input-addon" id="couponSubmit" onclick="Coupons.couponSubmit();"> Применить </button>')
                }
            }
        });
    },
    couponSubmit : function () {
        let coupon = $('#coupon').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        if (coupon) {
            $.ajax({
                url:"setCoupon/" + coupon,
                method:"POST",
                data:{
                    'coupon' : coupon
                },
                success: function(data) {
                    if (data.message == 'error') {
                        $('.coupon-caption').text('Купон не активен');
                        toastr.info('Купон не активен');
                    } else {
                        toastr.info('Купон применен');
                        $('.coupon-success').html("Купон применен: " + data['result']['coupon']['caption'] + "<br> (скидка не действует на акционные товары)");
                        $('.coupon-caption').empty();
                        $('.discount-block').empty();
                        $('.discount-block-left').empty();
                        $('.discount-block').append('<div>Скидка:</div><div class="discount-item">'+ parseInt(data['result']['total']['couponDiscount']) + ' р.</div>');
                        $('.discount-block-left').append('<div class="discount-item-desc">Скидка:</div><div class="discount-item">'+ parseInt(data['result']['total']['couponDiscount']) + ' р.</div>');
                        $('.total-item').text(parseInt(data['result']['total']['total']) + ' р.');
                        $('#coupon-buttons').html('<button class="btn btn-primary custom-button rich-input-addon" id="couponDestroy" onclick="Coupons.couponDestroy();"> Удалить </button>')
                    }
                }
            });
        }
    }
};

var Postcards = {
    setWishes : function (rowId, text) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url:"setWishes",
            method:"POST",
            data:{
                'rowId' : rowId,
                'text' : text
            },
            success: function(data) {
                // console.log(data);
            }
        });
    },
    use : function (rowId) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url:"usePostCard/" + rowId,
            method:"POST",
            data:{
                'id' : rowId
            },
            success: function(data) {
                $('.postcard-item').text(parseInt(data['postCard']) + ' р.');
                $('.total-item').text(parseInt(data['total']) + ' р.');
            }
        });
    }
};

var Couriers = {
    setCourier : function (courierId, isFree = 0) {
        (isFree == 1) ? $('.address-hide-wrap').addClass('hide') : $('.address-hide-wrap').removeClass('hide');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url:"courierChange/" + courierId,
            method:"POST",
            data:{
                'id' : courierId
            },
            success: function(data) {
                $('.delivery-item').text(parseInt(data['shippingFee']) + ' р.');
                $('.total-item').text(parseInt(data['total']) + ' р.');
            }
        });
    }
}

jQuery(document).ready(function ($) {
    localStorage.clear();
    /*
     * Select2
     */

    $(".custom-select").select2({
        width: 250,
        minimumResultsForSearch: -1,
        selectionCssClass: "custom-select-selection",
        dropdownCssClass: "custom-select-dropdown",
    });

    $(".account-wrapper .custom-select").select2({
        width: 290,
        minimumResultsForSearch: -1,
        selectionCssClass: "custom-select-selection",
        dropdownCssClass: "custom-select-dropdown",
    });

    $(".custom-select-account").select2({
        width: "100%",
        minimumResultsForSearch: -1,
        selectionCssClass: "custom-select-selection",
        dropdownCssClass: "custom-select-dropdown",
    });


    // tostr settings
    tostr: $(function () {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });
    }),


        /*
         * DatePicker
         */

    $('[data-toggle="featured"]').click(function (e) {
        e.preventDefault();
        let like = $(this);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/addToFavorite/' + $(this).data('id'),
            data: like.data('id'),
            success: function (data) {
                console.log(data);
                switch (data.message) {
                    case 'error':
                        toastr.info('Войдите в личный кабинет, чтобы сохранить товар в избранном </br><a href="/login">Войти</a>');
                        break;
                    case 'success':
                        toastr.info('Все избранные товары вы можете посмотреть в своём </br><a href="/accounts?tab=favorites">Личном кабинете</a>');
                        like.toggleClass("checked");
                        break;
                    case 'deleted':
                        toastr.info('Товар удален из избранного');
                        like.toggleClass("checked");
                        like.closest('.account-favorite-item').remove();
                        break;
                    default:
                        break;
                }
            }
        });

    });

    function updateQty(input, qty) {
        let rowId = input.data('id');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url:"cart/" + rowId,
            method:"POST",
            data:{
                _method: "PATCH",
                quantity : qty
            },
            success: function(data) {
                // обновление карточки товара
                input.val(data['product']['qty']);
                input.closest('.cart-prod-qty-group').find('.cart-product-subtotal').text(data['product']['price']*data['product']['qty'] + ' р.');
                input.closest('.cart-prod-wrapper').next('.cart-envelope').find('.cart-envelope-price').text(data['product']['qty']*data['product']['options']['postCardPrice'] + ' р.');

                // обновление итоговой цены
                $('.count-items').text(data['totalPrices']['countItems']);
                $('.postcard-item').text(parseInt(data['totalPrices']['postCard']) + ' р.');
                $('.subtotal-item').text(parseInt(data['totalPrices']['subTotal']) + ' р.');
                $('.cart-coupon-group-price').text(parseInt(data['totalPrices']['subTotal']) + ' р.');
                if(parseInt(data['totalPrices']['couponDiscount']) != 0) {
                    $('.discount-block').empty();
                    $('.discount-block').append('<div>Скидка:</div><div class="discount-item">'+ parseInt(data['totalPrices']['couponDiscount']) + ' р.</div>');
                }
                $('.total-item').text(parseInt(data['totalPrices']['total']) + ' р.');

            }
        });
    }

    $('.minus').click(function () {
        let input = $(this).closest('.cart-prod-qty-wrapper').find('input');
        let qty = parseInt(input.val()) - 1;
        if (qty != 0) {
            updateQty(input, qty);
        }
    });

    $('.plus').click(function () {
        let input = $(this).closest('.cart-prod-qty-wrapper').find('input');
        let qty = parseInt(input.val()) + 1;
        if (qty != 11) {
            updateQty(input, qty);
        }
    });


    $(".form-control-date").datepicker({
        minDate: new Date(),
    });

    /*
     * 02 Swiper sliders
     */

    function CategoriesSliderInit() {
        if ($(window).width() < 768) {
            window.CategoriesSlider = new Swiper($("#categories"), {
                wrapperClass: "categories-wrapper",
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },
            });
        } else {
            if (typeof CategoriesSlider != "undefined") {
                CategoriesSlider.destroy();
            }
        }
    }

    $(window).on("resize", CategoriesSliderInit);

    CategoriesSliderInit();

    function ProductSliderInit() {
        if ($(window).width() < 768) {
            window.ProductSlider = new Swiper($("#productSlider"), {
                wrapperClass: "categories-wrapper",
                pagination: {
                    el: ".swiper-pagination-custom",
                    clickable: true,
                },
            });
        } else {
            if (typeof ProductSlider != "undefined") {
                ProductSlider.destroy();
            }
        }
    }

    $(window).on("resize", ProductSliderInit);

    ProductSliderInit();

    /*
     * 03 Mobile menu
     */

    $("#mobile-nav-toggler").on("click", function () {
        $(this).toggleClass("active");
        $(".mobile-nav__body,.mobile-nav").toggleClass("active");
    });

    function button_group_switch() {
        const buttonGroups = $(".button-group");

        buttonGroups.each((key, group) => {
            const buttonItem = $(group).children();

        buttonItem.click(function () {
            let attr_from = $(this).data("from")
            let attr_id = $(this).data("id")
            let attr_procent = parseInt($(this).data("procent"))

            if (attr_from == 'feature') {
                if (localStorage.getItem("price") === null) {
                    localStorage.setItem("price", parseInt($(this).closest('.account-favorite-item').find('.current-price').text()));
                }

                $(this).closest('.account-favorite-item').find('.productAttribute').val(attr_id)
                $(this).closest('.account-favorite-item').find('.current-price').text(localStorage.getItem("price")*attr_procent/100)

            } else {
                if (localStorage.getItem("price") === null) {
                    localStorage.setItem("price", parseInt($('#price').text()));
                }
                if (localStorage.getItem("sale_price") === null) {
                    localStorage.setItem("sale_price", parseInt($('#sale_price').text()));
                }
                $('#productAttribute').val(attr_id)

                $('#price').text(localStorage.getItem("price")*attr_procent/100)
                $('#sale_price').text(localStorage.getItem("sale_price")*attr_procent/100)
            }
            $(this).addClass("button-group-item-active");
            buttonItem.not($(this)).removeClass("button-group-item-active");
        });
    });
    }

    button_group_switch();

    button_group_switch();

    function menu_switch() {
        const customMenus = $(".custom-menu");

        customMenus.each((key, menu) => {
            const menuControls = $(menu)
                .children(".menu-controls")
                .children("button");
        const menuContent = $(menu).children(".menu-content").children();

        menuControls.click(function () {
            $(this).addClass("menu-button-active");
            menuControls.not($(this)).removeClass("menu-button-active");
            menuContent.css("display", "none");
            const curContent = menuContent[menuControls.index($(this))];
            $(curContent).css("display", "block");
        });
    });
    }

    menu_switch();

    function get_url_params() {
        var params = window.location.search
            .replace("?", "")
            .split("&")
            .reduce(function (p, e) {
                var a = e.split("=");
                p[decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
                return p;
            }, {});

        return params;
    }

    // function account_menu_active() {
    //     const currentOrders = $("#current-orders");
    //     const orderHistory = $("#order-history");
    //     const personalData = $("#personal-data");
    //     const favorites = $("#favorites");
    //
    //     const items = [currentOrders, orderHistory, personalData, favorites];
    //
    //     const urlParams = get_url_params();
    //
    //     items.forEach((target) => {
    //         if (target.attr("id") === urlParams.page) {
    //         target.addClass("button-group-item-active");
    //         target.removeAttr("onclick");
    //     }
    // });
    // }
    //
    // account_menu_active();

    $('.admin-nav').click(function (event) {
        $('.admin-nav').removeClass('button-group-item-active');
        $(this).addClass("button-group-item-active");
    });

    $('.collapse-order').click(function () {
       var orderNumber = $(this).closest('account-order-header').find('.account-order-number');
       var transform = $(this).css('transform');
        if (transform === "none") {
            $(this).css("transform", "rotate(180deg)");
            orderNumber.css("color", "initial");
        } else {
            $(this).css("transform", "none");
            orderNumber.css("color", "#C60077");
        }
    });

    function show_password() {
        const richInput = $(".rich-input-password");
        richInput.each(function () {
            const richInputField = $(this).children(".rich-input-field");
            const richInputAddon = $(this).children(".rich-input-addon");
            richInputAddon.click(() => {
                if (richInputField.attr("type") === "password") {
                richInputField.attr("type", "text");
                richInputAddon.attr(
                    "src",
                    "../icons/see-inactive.svg"
                );
            } else {
                richInputField.attr("type", "password");
                richInputAddon.attr("src", "../icons/see.svg");
            }
        });
        });
    }

    show_password();

});