"use strict";
var multiItemSlider = (function () {
    return function (selector, config) {
        var _mainElements = document.querySelectorAll(selector); // РѕСЃРЅРѕРІРЅС‹Р№ СЌР»РµРјРµРЅС‚ Р±Р»РѕРєР°
        _mainElements.forEach((_mainElement) => {
            var _sliderWrapper = _mainElement.querySelector(".slider__wrapper"), // РѕР±РµСЂС‚РєР° РґР»СЏ .slider-item
            _sliderItems = _mainElement.querySelectorAll(".slider__item"), // СЌР»РµРјРµРЅС‚С‹ (.slider-item)
            _sliderControls = _mainElement.querySelectorAll(".slider__control"), // СЌР»РµРјРµРЅС‚С‹ СѓРїСЂР°РІР»РµРЅРёСЏ
            _sliderControlTop = _mainElement.querySelector(".slider__control_top"), // РєРЅРѕРїРєР° "TOP"
            _sliderControlBottom = _mainElement.querySelector(
                ".slider__control_bottom"
            ), // РєРЅРѕРїРєР° "BOTTOM"
            _sliderSelectedImage = _mainElement.querySelector(
                ".slider__selected-img"
            ), // РІС‹Р±СЂР°РЅРЅРѕРµ РёР·РѕР±СЂР°Р¶РµРЅРёРµ
            _sliderSelectedLightbox = _mainElement.querySelector(
                ".slider__selected-lightbox"
            ), // lightbox РґР»СЏ РІС‹Р±СЂР°РЅРЅРѕРіРѕ РёР·РѕР±СЂР°Р¶РµРЅРёСЏ
            _wrapperHeight = parseFloat(getComputedStyle(_sliderWrapper).height), // РІС‹СЃРѕС‚Р° РѕР±С‘СЂС‚РєРё
            _itemHeight = parseFloat(getComputedStyle(_sliderItems[0]).height) + 12, // РІС‹СЃРѕС‚Р° РѕРґРЅРѕРіРѕ СЌР»РµРјРµРЅС‚Р°
            _positionTopItem = 0, // РїРѕР·РёС†РёСЏ РІРµСЂС…РЅРµРіРѕ Р°РєС‚РёРІРЅРѕРіРѕ СЌР»РµРјРµРЅС‚Р°
            _transform = 0, // Р·РЅР°С‡РµРЅРёРµ С‚СЂР°РЅС„СЃРѕС„СЂРјР°С†РёРё .slider_wrapper
            _step = (_itemHeight / _wrapperHeight) * 100, // РІРµР»РёС‡РёРЅР° С€Р°РіР° (РґР»СЏ С‚СЂР°РЅСЃС„РѕСЂРјР°С†РёРё)
            _items = []; // РјР°СЃСЃРёРІ СЌР»РµРјРµРЅС‚РѕРІ
        // РЅР°РїРѕР»РЅРµРЅРёРµ РјР°СЃСЃРёРІР° _items
        _sliderItems.forEach(function (item, index) {
            _items.push({ item: item, position: index, transform: 0 });
        });

        _sliderItems.forEach(function (target) {
            target.onclick = function () {
                _sliderItems.forEach(function (other) {
                    other.style.border = "none";
                });
                target.style.border = "2px solid #C60077";
                _sliderSelectedImage.style.opacity = "0";
                setTimeout(() => {
                    _sliderSelectedImage.src = target.src;
                _sliderSelectedLightbox.href = target.src;
                _sliderSelectedImage.style.opacity = "1";
            }, 300);
            };
        });

        var position = {
            getMin: 0,
            getMax: _items.length - 1,
        };

        var _transformItem = function (direction) {
            if (direction === "bottom") {
                if (
                    _positionTopItem + _wrapperHeight / _itemHeight - 1 >=
                    position.getMax
                ) {
                    return;
                }
                if (
                    _sliderControlTop.classList.contains("slider__control_top-inactive")
                ) {
                    _sliderControlTop.classList.add("slider__control_top-active");
                    _sliderControlTop.classList.remove("slider__control_top-inactive");
                }
                if (
                    _sliderControlBottom.classList.contains(
                        "slider__control_bottom-active"
                    ) &&
                    _positionTopItem + _wrapperHeight / _itemHeight >= position.getMax
                ) {
                    _sliderControlBottom.classList.remove(
                        "slider__control_bottom-active"
                    );
                    _sliderControlBottom.classList.add(
                        "slider__control_bottom-inactive"
                    );
                }
                _positionTopItem++;
                _transform -= _step;
            }
            if (direction === "top") {
                if (_positionTopItem <= position.getMin) {
                    return;
                }
                if (
                    _sliderControlBottom.classList.contains(
                        "slider__control_bottom-inactive"
                    )
                ) {
                    _sliderControlBottom.classList.add("slider__control_bottom-active");
                    _sliderControlBottom.classList.remove(
                        "slider__control_bottom-inactive"
                    );
                }
                if (
                    _sliderControlTop.classList.contains(
                        "slider__control_top-active"
                    ) &&
                    _positionTopItem - 1 <= position.getMin
                ) {
                    _sliderControlTop.classList.remove("slider__control_top-active");
                    _sliderControlTop.classList.add("slider__control_top-inactive");
                }
                _positionTopItem--;
                _transform += _step;
            }
            _sliderWrapper.style.transform = "translateY(" + _transform + "%)";
        };

        // РѕР±СЂР°Р±РѕС‚С‡РёРє СЃРѕР±С‹С‚РёСЏ click РґР»СЏ РєРЅРѕРїРѕРє "РЅР°Р·Р°Рґ" Рё "РІРїРµСЂРµРґ"
        var _controlClick = function (e) {
            if (e.target.classList.contains("slider__control")) {
                e.preventDefault();
                var direction = e.target.classList.contains("slider__control_bottom")
                    ? "bottom"
                    : "top";
                _transformItem(direction);
            }
        };

        var _setUpListeners = function () {
            // РґРѕР±Р°РІР»РµРЅРёРµ Рє РєРЅРѕРїРєР°Рј "РЅР°Р·Р°Рґ" Рё "РІРїРµСЂРµРґ" РѕР±СЂР±РѕС‚С‡РёРєР° _controlClick РґР»СЏ СЃРѕР±С‹С‚СЏ click
            _sliderControls.forEach(function (item) {
                item.addEventListener("click", _controlClick);
            });
        };

        // РёРЅРёС†РёР°Р»РёР·Р°С†РёСЏ
        _setUpListeners();

        return {
            bottom: function () {
                // РјРµС‚РѕРґ bottom
                _transformItem("bottom");
            },
            top: function () {
                // РјРµС‚РѕРґ top
                _transformItem("top");
            },
        };
    });
    };
})();

var slider = multiItemSlider(".img-slider__container");